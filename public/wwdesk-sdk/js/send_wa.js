var appsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var initialAppsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var editorExtensionId = "haphhpfcpfeagcmannpebjjjpdlbhflh";
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var safestringdiv = $('<div>');
var extensionVersion = null;

function valueExists(val) {
    return val !== null && val !== undefined && val.length > 0 && val!=="null";
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Plivo Auth ID or Authtoken is Invalid <br><br> Try again with proper Plivo credentials from <a href="https://console.plivo.com/dashboard/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo dashboard</a>.');
}
function showContactsSearchPopUp(){
    $('.item-list-popup').show();
}
function searchContact(){
    searchContactsAndRender($('#contact-seach-name').val());
}
function searchContactsAndRender(searchTerm){
    if(!valueExists(searchTerm)){
        showErroWindow("Error","Enter search term");
        return;
    }
    var contactProcess = curId++;
    showProcess(`Searching contacts having phone numbers ...`, contactProcess);
                    var reqObj = {
                        url : `https://desk.zoho.com/api/v1/contacts/search?_all=*${encodeURI(searchTerm)}*&limit=99&mobile=`+encodeURI('${notEmpty}'),
                        connectionLinkName : "readreceiptconnection",
                        type : "GET", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{}
                     };
                     ZOHODESK.request(reqObj).then(function(response){
                         $("#contact-search-items").html(" ");
                         processCompleted(contactProcess);
                         var responseJSON = JSON.parse(response);
                         if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                             var data = JSON.parse(responseJSON["response"]).statusMessage;
                             if(data === ""){
                                 $("#contact-search-items").html("No contacts found. <br><br> <span class=\"c-silver\"> This search will include <br> only contacts having mobile numbers</span>");
                                 return;
                             }
                             if(!(data instanceof Object)){
                                 data = JSON.parse(data);
                             }
                             
                             var contactObjArray = data.data;
                             for(var i in contactObjArray){
                                 var obj = contactObjArray[i];
                                 var fullName = getSafeString((valueExists(obj.firstName) ? obj.firstName : "") +" " + (valueExists(obj.lastName) ? obj.lastName : ""));
                                 var itemHTML = `<div class="item-list-popup-item">
                    <span class="c-name"> ${fullName} </span> ${valueExists(obj.mobile) ? '<span class="c-phone" onclick="selectPhoneNumber(\''+obj.mobile+'\')"> <i class="material-icons">phone_iphone</i> '+obj.mobile+'</span>' : ''} ${valueExists(obj.phone) ? '<span class="c-phone" onclick="selectPhoneNumber(\''+obj.phone+'\')"> <i class="material-icons">phone_local</i> '+obj.phone+'</span>' : ''}
                </div>`;
                                 $("#contact-search-items").append(itemHTML);
                             }
                         }
                     }).catch(function(err){
                         processCompleted(contactProcess);
                         console.log(err);
                     });
                }

function selectPhoneNumber(number){
    $('#input-to-phone').val(number.replace(/\D/g,''));
    hideElem('.item-list-popup');
}
function sendSMS(){
    var prevVal = $('#input-to-phone').val();
    var phoneNumber = prevVal.replace(/\D/g,'');
    $('#input-to-phone').val(phoneNumber);
//    if(!valueExists(phoneNumber)){
//        showErroWindow("Enter To Phone-Number","Enter recipient phone number and then proceed");
//        return;
//    }
    if(!valueExists($("#input-sms-content").val().trim())){
        showErroWindow("Enter Message Content","Enter message content and then proceed");
        return;
    }
    var content = $('#input-sms-content').val();
    if(extensionVersion){
				var actmessage = {};
				actmessage[(Date.now())]={"n": prevVal,"m":content,"u":"https://desk.zoho.com","dt":(Date.now())};
				chrome.runtime.sendMessage(editorExtensionId, actmessage,function(response) {
										if (!response.success){
										  //handleError(url);
										}
                                        alert("Your message will be sent in background");
                                        $('#input-sms-content').val("");
									});
			}else{
            var url = "https://web.whatsapp.com/send?phone="+phoneNumber+"&text="+encodeURIComponent(content.trim());
            var win = window.open(url, '_blank');
            }
}
function resetSearch(){
    $("#contact-seach-name").val("");
    $("#contact-search-items").html("");
}
function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}
function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function getSafeString(str){
    return safestringdiv.text(str).html();
}
window.onload = function () {
    $('body').append(`<div class="help-dialog-text" style="
    line-height: 1.1;
"><div class="contactme" style="padding-bottom:10px">If you face any issues, kindly mail to <a href="mailto:ulgebra@zoho.com">ulgebra@zoho.com</a> or <a target="_blank" href="https://wa.me/916381504050?text=Hello%20Ulgebra,%20I%20have%20a%20query">WhatsApp Now</a><br><div class="ua-brand" style="color: grey;position: fixed;bottom: 10px;left:20px/* display: none; */"> <a href="https://www.ulgebra.com/?source=appsettings" target="_blank" style="
    color: grey;
    /* display: none; */
    text-decoration: none;
"><span class="brand-brand"> <img src="https://ulgebra.com/images/ulgebra/favs/favicon.png" style="
    margin-right: 10px;
    vertical-align: middle;
    margin-top: -2px;
"></span>ulgebra.com</a></div></div></div>`);
checkVersion();
    ZOHODESK.extension.onload().then(function (App) {
        console.log(App);
        $("#input-from-phone").hide();
        appsConfig.EXTENSION_ID = App.extensionID;

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });
        ZOHODESK.get("portal.name").then(function (response) {
            appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
        }).catch(function (err) {
            console.log(err);
        });
        ZOHODESK.get("portal.customDomainName").then(function(response){
            appsConfig.UA_DESK_CUSTOMDOMAIN_NAME = response['portal.customDomainName'];
        }).catch(function(err){
            console.log(err);
        });

        ZOHODESK.get("user").then(function(userResponse) {
            userEmail = (userResponse.user.email);
            ZOHODESK.get("portal").then(function(portalResponse) {
             portalName = (portalResponse.portal.name);
             $("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=cx-mail:${userEmail}:::-cx-oname:${portalName}::" />`);
            });
        });

    });
    
    $('#sms-form .btn-save').hide();
    $('#input-to-phone').after(`<div class="inp-botton-tip" style="font-size: 12px;color: #8c8c8c;float: left;">Leave it as empty to send to multiple people</div>`);
            //$("#dropdown_text").on('click', app.checkIfSnippetExistsAndDeleteThenCreate);
            $('#input-sms-content').on('input', (function(){
                if($('#input-sms-content').val().trim().length>0 && $('#input-sms-content').val().trim().length>0){
                    $('#sms-form .btn-save').show();
                }else{
                	$('#sms-form .btn-save').hide();
                }
            }));
};

function checkVersion(){
			if(!!window.chrome){
				chrome.runtime.sendMessage(editorExtensionId, { "message": "version" },function (reply){
					console.log(reply);
					extensionVersion = reply;
					if(!extensionVersion){
						showNoChromeExtError();
					}
				});	
				}else{
				showNoChromeExtError();
			}		
		}


function showNoChromeExtError(){
			$("body").append(`<div class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                Install Google Chrome Extension! <div class="cetp-close" onclick="$('.chromeextnpopup').hide()">x</div>
            </div>
            <div class="cetp-content">
                Our Google Chrome extension is needed to Send Messages in Background. <br><br>
                <a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/haphhpfcpfeagcmannpebjjjpdlbhflh" target="_blank"> 
                    <button class="cetp-install">
                        <span class="material-icons">get_app</span> Install Google Chrome Extension
                    </button>
                </a>
            </div>
        </div>
    </div>`);
		}
