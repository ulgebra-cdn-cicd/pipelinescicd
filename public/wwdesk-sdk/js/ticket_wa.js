			
	var appsConfig = {
		"UA_USER_ID": undefined,
		"UA_DESK_ORG_ID" : undefined,
		"UA_CF_CREATED" : false,
		"UA_SNIPPET_CREATED" : false
	};
var editorExtensionId = "haphhpfcpfeagcmannpebjjjpdlbhflh";
	var portalName = "";
	var userEmail = "";
	var extensionVersion = null;
	var dropDown = (function(config){
      let appDropDown = {
          config : config,
          init : function(config = null){
			this.config = config ? config : this.config;
			config = this.config;
			
            let len = this.config.values.length;
			let parent = document.createElement('div');
			
			for(let index in config.values){
				let div = document.createElement('div');
				let cont_elem = document.createElement('span');
				let num_val =  config.values[index].snippetId;;
				cont_elem.innerText = config.values[index].snippetWord;

				cont_elem.setAttribute('class',this.config.name + "_item");
				cont_elem.setAttribute('data-' + this.config.name,num_val);
				div.appendChild(cont_elem);
				parent.appendChild(div);
            }
            
            this.config.input.parentNode.appendChild(parent);

            parent.classList.add('dropDownContainer');
            parent.classList.add(this.config.name);
            
            this.config.input.addEventListener('click',appDropDown.ShowOrHide);

			$('.dropDownContainer .' + appDropDown.config.name + "_item").parent().click(function(e){
				$('.snippet_added_text').hide();
				let snippetId = $(this).find('span').attr('data-' + appDropDown.config.name);
				let snippetName = snippetId.length ? appDropDown.config.values.filter(o=>o.snippetId == snippetId) : [];
					snippetName = snippetName.length ? snippetName[0].snippetWord : "";
				if(snippetId){

					app.replaceSnippetContent(snippetId,appDropDown.config.target);
					$('#dropdown_text').text(snippetName);

				}
				
				appDropDown.ShowOrHide(e);

            });

            $('.dropDownContainer.' + appDropDown.config.name).hide();

            this.set_blur_hide();
          },
          ShowOrHide : function(e){
		  
            $('.dropDownContainer').not($('.dropDownContainer.' + appDropDown.config.name)).hide();
			$('.dropDownContainer.' + appDropDown.config.name).toggle();
			appDropDown.config.input.classList.toggle('open');

            e.stopPropagation();
          },
          set_blur_hide : function(){
            let close = 1;
            $('body').on('click',function(){
              if(close){
				  $('.dropDownContainer.' + appDropDown.config.name).hide();
				  appDropDown.config.input.classList.remove('open');
			  }
              close = 1;
            });

            $('.dropDownContainer.' + appDropDown.config.name).on('mousedown',function(){
              close = 0;
            });

            $(appDropDown.config.input).on('blur',function(){
			  if(close){
				  $('.dropDownContainer.' + appDropDown.config.name).hide();
				  appDropDown.config.input.classList.remove('open');
			  }
             close = 1;
            });
          },
         
      }
      appDropDown.init();
    });
	
	let app = {
            stripHtml : function(html)
                {
                    console.log(html);
                    html = html.replace(/<div>/g," ").replace(/<\/div>/g,"&#13;&#10;");
                   var tmp = document.createElement("DIV");
                   tmp.innerHTML = html;
                   return tmp.textContent || tmp.innerText || "";
                },
		setTicketComment : function(snippet_content){
			console.log('set');
                        let msg_content = this.stripHtml(snippet_content);
			$('#input-sms-content').val(msg_content);
			$('.btn-save').show();
		},
		replaceSnippetContent : function(snippetId,target_elem){
			ZOHODESK.get("ticket").then(function(response){
				let ticket_id = response.ticket.id;
				var reqObj = {
				url : `https://desk.zoho.com/api/v1/mysnippets/${snippetId}/apply?caseId=${ticket_id}`,
				type : "POST", 
                                connectionLinkName: "readreceiptconnection",
				headers:{
					"orgId" : appsConfig.UA_DESK_ORG_ID
				},
				postBody:{}
				};
				$(".snippet_added_text").text("Processing snippet...").show();
				ZOHODESK.request(reqObj).then(function(response){
					var responseJSON = JSON.parse(response);
                                var data = {};
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                data = JSON.parse(responseJSON["response"]).statusMessage;
                                console.log(data);
                            }
                            $(".snippet_added_text").text("Your snippet has been pasted in editor");
					app.showSuccess = 1;
					app.setTicketComment(data.replacedContent);
				})

			});
		},
		checkIfSnippetExistsAndDeleteThenCreate : function(){
				
			var reqObj = {
			url : `https://desk.zoho.com/api/v1/mysnippets`,
			type : "GET", 
                        connectionLinkName: "readreceiptconnection",
			headers:{
				"orgId" : appsConfig.UA_DESK_ORG_ID
			},
			postBody:{}
			};
			
			ZOHODESK.request(reqObj).then(function(response){

				var responseJSON = JSON.parse(response);
                                var data = {};
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                data = JSON.parse(JSON.parse(responseJSON["response"]).statusMessage);
                                console.log(data);
                            }
				if(data.length === 0){
					$("#dropdown_text").text("No Snippets Found");
					$("#selSnippet .arrow.down").hide();
				}
				let snippets = {
					name : 'my_snippets',
					target : document.getElementById('template_content'),
					input : document.getElementById('selSnippet'),
					values : data,
				};
				
				dropDown(snippets);

			}).catch(function(err){
				console.log(err);
			});

		},
		initialize : function(){
			
			ZOHODESK.extension.onload().then(function (App) {
				ZOHODESK.get("extension.config").then(function(response){
					var data = response['extension.config'];
					for (var item in data) {

						var configname = data[item]['name'];
						var configValue = data[item]['value'];
						
						if(configname==='UA_USERID'){
							appsConfig.UA_USER_ID = configValue;
							$('#input-api-key').val(appsConfig.UA_USER_ID);
						}
						
						if(configname==='UA_CF_CREATED'){
							appsConfig.UA_CF_CREATED = configValue;
						}
						
						if(configname==='UA_SNIPPET_CREATED'){
							appsConfig.UA_SNIPPET_CREATED = configValue;
						}
						resolveCurrentProcessView();
					}

				}).catch(function(err){
					console.log(err);
				});

				ZOHODESK.get("portal.id").then(function(response){
					if(response){
						appsConfig.UA_DESK_ORG_ID = response['portal.id'];
						app.checkIfSnippetExistsAndDeleteThenCreate();
					}
				}).catch(function(err){
						console.log(err);
				});	
				
			});
		}
	};
	window.onload = function () {
	    $('body').append(`<div class="help-dialog-text" style="
    background-color:whitesmoke;
"><div class="contactme" style="padding-bottom:10px;line-height:1.4">If you face any issues, kindly mail to <a href="mailto:ulgebra@zoho.com">ulgebra@zoho.com</a> or <a target="_blank" href="https://wa.me/916381504050?text=Hello%20Ulgebra,%20I%20have%20a%20query">WhatsApp Now</a><br></div></div>`);
            app.initialize();
            $('.btn-save').hide();
            //$("#dropdown_text").on('click', app.checkIfSnippetExistsAndDeleteThenCreate);
            $('#input-sms-content').on('input', (function(){
                if($('#input-sms-content').val().trim().length>0 && $('#input-sms-content').val().trim().length>0){
                    $('.btn-save').show();
                }else{
                	$('.btn-save').hide();
                }
            }));
            
            
    ZOHODESK.extension.onload().then(function (App) {
      
        setTicketPhoneNumberInField();
        console.log(App);
		checkVersion();
        ZOHODESK.get("user").then(function(userResponse) {
		 	userEmail = (userResponse.user.email);
		 	ZOHODESK.get("portal").then(function(portalResponse) {
			 portalName = (portalResponse.portal.name);
			 $("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=cx-mail:${userEmail}:::-cx-oname:${portalName}::" />`);
			});
		});
        

    });
};

function setTicketPhoneNumberInField(){
    ZOHODESK.get("ticket").then(function (response) {
        var ticketPhone = response['ticket']['phone'];
        if(valueExists(ticketPhone)){
            ticketPhone = ticketPhone.replace(/\D/g,'');
            $("#input-to-phone").val(ticketPhone);
        }else{
            var reqObj = {
				url : "https://desk.zoho.com/api/v1/contacts/"+response['ticket']['contactId'],
				type : "GET", 
                                connectionLinkName: "readreceiptconnection",
				headers:{
					"orgId" : appsConfig.UA_DESK_ORG_ID
				},
				postBody:{}
			};

			ZOHODESK.request(reqObj).then(function(c){
				var ticketPhone = valueExists(response['mobile']) ? response['mobile'] : response['phone'];
                                if(valueExists(ticketPhone)){
                                    ticketPhone = ticketPhone.replace(/\D/g,'');
                                    $("#input-to-phone").val(ticketPhone);
                                }
                                
			}).catch(err=>{
				console.log(err);
			})
        }
    });
}

function valueExists(val) {
    return val !== null && val !== undefined && val.length > 0 && val!=="null";
}

function addAsComment(){
    var content = escapeHTMLS($('#input-sms-content').val()).replace(/(?:\r\n|\r|\n)/g, '<br>');
    ZOHODESK.set("ticket.comment",{"comment":content}).then(function(response){
				console.log(' aded ')
			}).catch(function(err){
				console.log('error aded ');
			});
			if(app.showSuccess)
				setTimeout(function(){
					$('.snippet_added_text').text('Your snippet has been pasted in comment editor').show();
				},200);
}
	
        
        function sendSMS(){
            var content = $('#input-sms-content').val();
            var prevVal = $('#input-to-phone').val();
    		var phoneNumber = prevVal.replace(/\D/g,'');
			if(extensionVersion){
				var actmessage = {};
				actmessage[Date.now()]={"n": prevVal,"m":content,"u":"https://desk.zoho.com","dt": Date.now()};
				chrome.runtime.sendMessage(editorExtensionId, actmessage,function(response) {
										if (!response.success){
										  //handleError(url);
										}
										$('#input-sms-content').val("");
										alert("Your message will be sent in background");
									});
			}else{
				var url = "https://web.whatsapp.com/send?phone="+phoneNumber+"&text="+encodeURIComponent(content);
            	var win = window.open(url, '_blank');
			}
        }
        
        function escapeHTMLS(rawStr){
              return $('<textarea/>').text(rawStr).html();
          }


		  function checkVersion(){
			if(!!window.chrome){
				chrome.runtime.sendMessage(editorExtensionId, { "message": "version" },function (reply){
					console.log(reply);
					extensionVersion = reply;
					if(!extensionVersion){
						showNoChromeExtError();
					}
				});	
				}else{
				showNoChromeExtError();
			}		
		}


function showNoChromeExtError(){
			$("body").append(`<div class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                Install Google Chrome Extension! <div class="cetp-close" onclick="$('.chromeextnpopup').hide()">x</div>
            </div>
            <div class="cetp-content">
                Our Google Chrome extension is needed to Send WhatsApp Messages in Background.<br><br>
                <a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/haphhpfcpfeagcmannpebjjjpdlbhflh" target="_blank">
                    <button class="cetp-install">
                        <span class="material-icons">get_app</span> Install Now
                    </button>
                </a>
            </div>
        </div>
    </div>`);
		}

	  
