var modules = [];
var workspaces = [];
var templates = [];
var params = {};
var templateName = "Default";
var selectedTemplate;
var selectedTemplateSettingValue = {};
var onEdit = 0;
var selectedId = "";
var optionsSelected = [];
var usersList = [];
var projectsList = [];
var tagsList = [];
var optCallback = (()=>{});
var zapikey;
var selectedModule;
var loadingId = 0;
var record;
var currentRecord;
document.addEventListener("DOMContentLoaded", function(event) {

	document.getElementById("loader").style.display = "none";
	document.getElementById("contentDiv").style.display = "block";
	document.getElementById("google_translate_element").style.display = "block";
	ZOHO.embeddedApp.on("PageLoad", function(response) {
		// record = {"EntityId":["4440792000000282037"],"Entity":"Leads","ButtonPosition":"DetailView"};
		record = response;
		var currentLoadingId1 = loadingId++;
		showProcess("Fetching record",currentLoadingId1)
		ZOHO.CRM.API.getRecord({Entity:record.Entity,RecordID:record.EntityId[0]}).then(function(response1){
			processCompleted(currentLoadingId1);
		    currentRecord = response1.data[0];
		    if (!Object.keys) {
			  Object.keys = function(obj) {
			    var keys = [];

			    for (var i in obj) {
			      if (obj.hasOwnProperty(i)) {
			        keys.push(i);
			      }
			    }

			    return keys;
			  };
			}
		    initialize();
		})
	});
	ZOHO.embeddedApp.init().then(function(){
		$('#datepicker').datepicker().datepicker('setDate',new Date());
		// document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
    }); 
});

function showProcess(text, id)
{
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function processCompleted(id)
{
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}

function showFollowersMultiSelect()
{
	for(var index in usersList)
	{
		var user = usersList[index];
		if (params['followers']) 
		{
			if(params['followers'].includes(user.value))
			{
				user.selected = true;
			}
			else
			{
				user.selected = false;
			}
		}
		else
		{
			user.selected = false;
		}
	}
	showMultiSelect('Choose followers',usersList, console.log);
}

function showProjectsMultiSelect()
{
	for(var index in projectsList)
	{
		var project = projectsList[index];
		if (params['projects']) 
		{
			if(params['projects'].includes(project.value))
			{
				project.selected = true;
			}
			else
			{
				project.selected = false;
			}
		}
		else
		{
			project.selected = false;
		}
	}
	showMultiSelect('Choose projects',projectsList, console.log);
	asanaApp.setSearchFilter();
}

function showTagsMultiSelect()
{
	for(var index in tagsList)
	{
		var tag = tagsList[index];
		if (params['tags']) 
		{
			if(params['tags'].includes(tag.value))
			{
				tag.selected = true;
			}
			else
			{
				tag.selected = false;
			}
		}
		else
		{
			tag.selected = false;
		}
	}
	showMultiSelect('Choose tags',tagsList, console.log);
}

function initialize(isEdit,createNewTemplate)
{
	$("#input-form").show();
	$("#save-options").show();
	var data = {};
	var currentLoadingId2 = loadingId++;
	showProcess("Fetching modules",currentLoadingId2)
	ZOHO.CRM.META.getModules().then(function(data){
		processCompleted(currentLoadingId2);
		var html = "";
		for(var index in data.modules)
		{
			var module = data.modules[index];
			if(module.creatable || module.deletable || module.editable)
			{
				modules.push(module);
				html = html + "<option value=" + module.id + ">" + safetext(module.singular_label) + "</option>";
			}
		}
		selectedModule = modules[findWithAttr(modules,"api_name",record.Entity)]
		constructParams("module",selectedModule.id);
		$("#fields-list").text(selectedModule.singular_label);
		$("#notes-fields-list").text(selectedModule.singular_label);
		$("#modules").html("");
		$("#modules").append(html);
	});
	
	var currentLoadingId3 = loadingId++;
	showProcess("Fetching workspaces",currentLoadingId3)
	ZOHO.CRM.CONNECTOR.invokeAPI("asanaforzohocrm.asanaconnector.getworkspaces",{}).then(function(data){
		processCompleted(currentLoadingId3);
		// data = {"status_code":200,"response":"{\"data\":[{\"gid\":\"1169906130665400\",\"name\":\"ulgebra.com\",\"resource_type\":\"workspace\"},{\"gid\":\"1169903779566511\",\"name\":\"workspace 2\",\"resource_type\":\"workspace\"}]}"};
		workspaces = JSON.parse(data.response).data;
		$("#workspaces").html("");
		var html = "";
		for(var index in workspaces)
		{
			var workspace = workspaces[index];
			html = html + "<option value=" + workspace.gid + ">" + safetext(workspace.name) + "</option>";
		}
		var workspaceIndex = 0;
		if(selectedTemplateSettingValue["workspace"])
		{
			workspaceIndex = findWithAttr(workspaces,"gid",selectedTemplateSettingValue["workspace"]);
		}
		$("#workspaces").append(html);
		constructParams("workspace",workspaces[workspaceIndex].gid);
	})
}

function showHoverableList(target, data, label="items"){
	var dataHTML = "";
	for (var i in data){
		dataHTML+=safetext(data[i])+"<br>";
	}
	$(target).append(`
		<div class="clickableListHolder"> <span class="clickListLabel">${data.length+" "+label} </span>
			<div class="clickableListItemsHolder">
				${dataHTML}
			</div> 
		</div>
	`);
}

function constructParams(param,value)
{
	if(value == "" || value == undefined)
	{
		delete params[param];
		if(param == "followers")
		{
			$("#followers-list").html("");
			delete params["followers_names"];
		}
		else if(param == "projects")
		{
			$("#projects-list").html("");
			delete params["projects_names"];
		}
		else if(param == "tags")
		{
			$("#tags-list").html("");
			delete params["tags_names"];
		}
		else if(param == "due_on")
		{
			hideDatePicker();
		}
	}
	else
	{
		if(param == "followers")
		{
			params["followers"] = [];
			params["followers_names"] = [];
			$("#followers-list").html("");
			for(var index in value)
			{
				if(index != 0)
				{
					//$("#followers-list").append(", ");
				}
				params["followers"].push(value[index]);
				params["followers_names"].push(getAttrforId(users,"name",value[index],"gid"))
				//$("#followers-list").append(safetext(getAttrforId(users,"name",value[index],"gid")));
			}
			showHoverableList("#followers-list", params["followers_names"], "followers selected");
		}
		else if(param == "projects")
		{
			params["projects"] = [];
			params["projects_names"] = [];
			$("#projects-list").html("");
			for(var index in value)
			{
				if(index != 0)
				{
					//$("#projects-list").append(", ");
				}
				params["projects"].push(value[index]);
				params["projects_names"].push(getAttrforId(projects,"name",value[index],"gid"))
				//$("#projects-list").append(safetext(getAttrforId(projects,"name",value[index],"gid")));
			}
			showHoverableList("#projects-list", params["projects_names"], "projects selected");
		}
		else if(param == "tags")
		{
			params["tags"] = [];
			params["tags_names"] = [];
			$("#tags-list").html("");
			for(var index in value)
			{
				if(index != 0)
				{
					//$("#tags-list").append(", ");	
				}
				params["tags"].push(value[index]);
				params["tags_names"].push(getAttrforId(tags,"name",value[index],"gid"))
				//$("#tags-list").append(safetext(getAttrforId(tags,"name",value[index],"gid")));
			}
			showHoverableList("#tags-list", params["tags_names"], "tags selected");
		}
		else
		{
			params[param] = value;
		}
	}
	if(param == "workspace")
	{
		getUsers(value);
		getProjects(value);
		getTags(value);
	}
	if(param == "module")
	{
		getFields(selectedModule.api_name);
	}
	console.log(params);
}

function getAttrforId(array,attr,id,searchParam)
{
	searchParam = searchParam?searchParam:"id";
	return array[findWithAttr(array,searchParam,id)][attr];
}

function findWithAttr(array, attr, value) 
{
    for(var i = 0; i < array.length; i += 1)
    {
        if(array[i][attr] === value) 
        {
        	return i;
        }
    }
 }

function getUsers(workspace_id)
{
	$("#users").html("");
	$("#users").append("<option value=''>Select Assignee</option>");
	constructParams("assignee","");
	var currentLoadingId5 = loadingId++;
	showProcess("Fetching users",currentLoadingId5)
	ZOHO.CRM.CONNECTOR.invokeAPI("asanaforzohocrm.asanaconnector.getusers",{"workspace_gid":workspace_id}).then(function(data){
		// var data = {"status_code":200,"response":"{\"data\":[{\"gid\":\"1169906208984281\",\"name\":\"mr_robot\",\"resource_type\":\"user\"},{\"gid\":\"1169906314708488\",\"name\":\"user1@ulgebra.com\",\"resource_type\":\"user\"}]}"};
		processCompleted(currentLoadingId5);
		users = JSON.parse(data.response).data;
		var html = "";
		usersList = [];
		for(var index in users)
		{
			var user = users[index];
			usersList.push({label: user.name,value: user.gid});
			html = html + "<option value=" + user.gid + ">" + safetext(user.name) + "</option>";
		}
		$("#users").append(html);
	})
}

function getProjects(workspace_id)
{
	constructParams("projects","");
	var currentLoadingId6 = loadingId++;
	showProcess("Fetching Projects",currentLoadingId6)
	ZOHO.CRM.CONNECTOR.invokeAPI("asanaforzohocrm.asanaconnector.getprojectsbyworkspace",{"workspace_gid":workspace_id}).then(function(data){
		// var data = {"status_code":200,"response":"{\"data\":[{\"gid\":\"1169903779566489\",\"name\":\"Sample\",\"resource_type\":\"project\"},{\"gid\":\"1169913284961163\",\"name\":\"Sample 2\",\"resource_type\":\"project\"},{\"gid\":\"1169913284961170\",\"name\":\"Sample 3\",\"resource_type\":\"project\"}]}"};
		processCompleted(currentLoadingId6)
		projects = JSON.parse(data.response).data;
		projects.sort((a, b) => {return a.name > b.name ? 1 : -1})
		if(projects.length)
		{
			$("#projects-not-available").hide()
			$("#projects").show()
		}
		else
		{
			$("#projects-not-available").show()
			$("#projects").hide()
		}
		projectsList = [];
		for(var index in projects)
		{
			var project = projects[index];
			projectsList.push({label: project.name,value: project.gid});
		}
	})
}

function getTags(workspace_gid)
{
	constructParams("tags","");
	var currentLoadingId7 = loadingId++;
	showProcess("Fetching tags",currentLoadingId7)
	ZOHO.CRM.CONNECTOR.invokeAPI("asanaforzohocrm.asanaconnector.gettagsbyworkspace",{"workspace_gid":workspace_gid}).then(function(data){
		// var data = {"status_code":200,"response":"{\"data\":[{\"gid\":\"1169903779566489\",\"name\":\"Sample\",\"resource_type\":\"project\"},{\"gid\":\"1169913284961163\",\"name\":\"Sample 2\",\"resource_type\":\"project\"},{\"gid\":\"1169913284961170\",\"name\":\"Sample 3\",\"resource_type\":\"project\"}]}"};
		processCompleted(currentLoadingId7);
		tags = JSON.parse(data.response).data;
		if(tags.length)
		{
			$("#tags-not-available").hide()
			$("#tags").show()
		}
		else
		{
			$("#tags-not-available").show()
			$("#tags").hide()
		}
		tagsList = [];
		for(var index in tags)
		{
			var tag = tags[index];
			tagsList.push({label: tag.name,value: tag.gid});
		}
	})
}

function getFields(module_name)
{
	var currentLoadingId8 = loadingId++;
	showProcess("Fetching fields",currentLoadingId8)
	ZOHO.CRM.META.getFields({"Entity":module_name}).then(function(data){
		// var data = {"fields":[{"system_mandatory":false,"webhook":true,"json_type":"jsonobject","crypt":null,"field_label":"Lead Owner","tooltip":null,"created_source":"default","field_read_only":true,"display_label":"Lead Owner","ui_type":8,"read_only":false,"association_details":null,"businesscard_supported":true,"currency":{},"id":"4440792000000002589","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"SMOWNERID","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Owner","unique":{},"data_type":"ownerlookup","formula":{},"category":1,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Company","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Company","ui_type":1,"quick_sequence_number":"1","read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002591","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":100,"column_name":"COMPANY","view_type":{"view":true,"edit":true,"quick_create":true,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Company","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":true,"webhook":true,"json_type":"string","crypt":null,"field_label":"Last Name","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Last Name","ui_type":127,"quick_sequence_number":"3","read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002595","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":80,"column_name":"LASTNAME","view_type":{"view":false,"edit":true,"quick_create":true,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Last_Name","unique":{},"data_type":"text","formula":{},"category":1,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"First Name","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"First Name","ui_type":27,"quick_sequence_number":"2","read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002593","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":40,"column_name":"FIRSTNAME","view_type":{"view":false,"edit":true,"quick_create":true,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"First_Name","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Email","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Email","ui_type":25,"quick_sequence_number":"4","read_only":false,"association_details":null,"businesscard_supported":true,"currency":{},"id":"4440792000000002599","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":100,"column_name":"EMAIL","view_type":{"view":true,"edit":true,"quick_create":true,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Email","unique":{},"data_type":"email","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":false,"json_type":"string","crypt":null,"field_label":"Full Name","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Full Name","ui_type":1,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002631","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"FULLNAME","view_type":{"view":true,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":0,"api_name":"Full_Name","unique":{},"data_type":"text","formula":{},"category":2,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Title","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Designation","ui_type":1,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002597","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":100,"column_name":"DESIGNATION","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Designation","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Phone","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Phone","ui_type":33,"quick_sequence_number":"5","read_only":false,"association_details":null,"businesscard_supported":true,"currency":{},"id":"4440792000000002601","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":30,"column_name":"PHONE","view_type":{"view":true,"edit":true,"quick_create":true,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Phone","unique":{},"data_type":"phone","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Mobile","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Mobile","ui_type":33,"read_only":false,"association_details":null,"businesscard_supported":true,"currency":{},"id":"4440792000000002605","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":30,"column_name":"MOBILE","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Mobile","unique":{},"data_type":"phone","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Lead Status","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Lead Status","ui_type":2,"read_only":false,"association_details":null,"businesscard_supported":true,"currency":{},"id":"4440792000000002611","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"STATUS","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Lead_Status","unique":{},"history_tracking":false,"data_type":"picklist","formula":{},"category":0,"decimal_place":null,"mass_update":true,"blueprint_supported":true,"pick_list_values":[{"display_value":"-None-","actual_value":"-None-","id":"4440792000000003409"},{"display_value":"Attempted to Contact","actual_value":"Attempted to Contact","id":"4440792000000003405"},{"display_value":"Contact in Future","actual_value":"Contact in Future","id":"4440792000000003401"},{"display_value":"Contacted","actual_value":"Contacted","id":"4440792000000003399"},{"display_value":"Junk Lead","actual_value":"Junk Lead","id":"4440792000000003411"},{"display_value":"Lost Lead","actual_value":"Lost Lead","id":"4440792000000003407"},{"display_value":"Not Contacted","actual_value":"Not Contacted","id":"4440792000000003403"},{"display_value":"Pre Qualified","actual_value":"Pre Qualified","id":"4440792000000003397"},{"display_value":"Not Qualified","actual_value":"Not Qualified","id":"4440792000000149001"}],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Fax","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Fax","ui_type":35,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002603","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":30,"column_name":"FAX","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Fax","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Website","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Website","ui_type":21,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002607","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":255,"column_name":"WEBSITE","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Website","unique":{},"data_type":"website","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Lead Source","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Lead Source","ui_type":2,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002609","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"LEADSOURCE","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Lead_Source","unique":{},"history_tracking":false,"data_type":"picklist","formula":{},"category":0,"decimal_place":null,"mass_update":true,"blueprint_supported":false,"pick_list_values":[{"display_value":"-None-","actual_value":"-None-","id":"4440792000000003391"},{"display_value":"Advertisement","actual_value":"Advertisement","id":"4440792000000003385"},{"display_value":"Cold Call","actual_value":"Cold Call","id":"4440792000000003389"},{"display_value":"Employee Referral","actual_value":"Employee Referral","id":"4440792000000003369"},{"display_value":"External Referral","actual_value":"External Referral","id":"4440792000000003379"},{"display_value":"OnlineStore","actual_value":"OnlineStore","id":"4440792000000003375"},{"display_value":"Partner","actual_value":"Partner","id":"4440792000000003377"},{"display_value":"Public Relations","actual_value":"Public Relations","id":"4440792000000003395"},{"display_value":"Sales Mail Alias","actual_value":"Sales Mail Alias","id":"4440792000000003393"},{"display_value":"Seminar Partner","actual_value":"Seminar Partner","id":"4440792000000003373"},{"display_value":"Seminar-Internal","actual_value":"Seminar-Internal","id":"4440792000000003383"},{"display_value":"Trade Show","actual_value":"Trade Show","id":"4440792000000003371"},{"display_value":"Web Download","actual_value":"Web Download","id":"4440792000000003381"},{"display_value":"Web Research","actual_value":"Web Research","id":"4440792000000003387"},{"display_value":"Chat","actual_value":"Chat","id":"4440792000000058001"}],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Industry","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Industry","ui_type":2,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002613","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"INDUSTRY","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Industry","unique":{},"history_tracking":false,"data_type":"picklist","formula":{},"category":0,"decimal_place":null,"mass_update":true,"blueprint_supported":false,"pick_list_values":[{"display_value":"-None-","actual_value":"-None-","id":"4440792000000003431"},{"display_value":"ASP","actual_value":"ASP","id":"4440792000000003439"},{"display_value":"Data/Telecom OEM","actual_value":"Data/Telecom OEM","id":"4440792000000003415"},{"display_value":"ERP","actual_value":"ERP","id":"4440792000000003441"},{"display_value":"Government/Military","actual_value":"Government/Military","id":"4440792000000003429"},{"display_value":"Large Enterprise","actual_value":"Large Enterprise","id":"4440792000000003421"},{"display_value":"ManagementISV","actual_value":"ManagementISV","id":"4440792000000003417"},{"display_value":"MSP (Management Service Provider)","actual_value":"MSP (Management Service Provider)","id":"4440792000000003435"},{"display_value":"Network Equipment (Enterprise)","actual_value":"Network Equipment (Enterprise)","id":"4440792000000003427"},{"display_value":"Non-management ISV","actual_value":"Non-management ISV","id":"4440792000000003433"},{"display_value":"Optical Networking","actual_value":"Optical Networking","id":"4440792000000003445"},{"display_value":"Service Provider","actual_value":"Service Provider","id":"4440792000000003419"},{"display_value":"Small/Medium Enterprise","actual_value":"Small/Medium Enterprise","id":"4440792000000003425"},{"display_value":"Storage Equipment","actual_value":"Storage Equipment","id":"4440792000000003437"},{"display_value":"Storage Service Provider","actual_value":"Storage Service Provider","id":"4440792000000003443"},{"display_value":"Systems Integrator","actual_value":"Systems Integrator","id":"4440792000000003413"},{"display_value":"Wireless Industry","actual_value":"Wireless Industry","id":"4440792000000003423"}],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"integer","crypt":null,"field_label":"No. of Employees","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"No of Employees","ui_type":32,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002615","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":9,"column_name":"EMPCT","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"No_of_Employees","unique":{},"data_type":"integer","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"double","crypt":null,"field_label":"Annual Revenue","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Annual Revenue","ui_type":36,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{"rounding_option":"normal","precision":2},"id":"4440792000000002617","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":16,"column_name":"ANNUALREVENUE","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Annual_Revenue","unique":{},"data_type":"currency","formula":{},"category":0,"decimal_place":2,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Rating","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Rating","ui_type":2,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002619","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"RATING","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Rating","unique":{},"history_tracking":false,"data_type":"picklist","formula":{},"category":0,"decimal_place":null,"mass_update":true,"blueprint_supported":true,"pick_list_values":[{"display_value":"-None-","actual_value":"-None-","id":"4440792000000003219"},{"display_value":"Acquired","actual_value":"Acquired","id":"4440792000000003221"},{"display_value":"Active","actual_value":"Active","id":"4440792000000003223"},{"display_value":"Market Failed","actual_value":"Market Failed","id":"4440792000000003225"},{"display_value":"Project Cancelled","actual_value":"Project Cancelled","id":"4440792000000003227"},{"display_value":"ShutDown","actual_value":"ShutDown","id":"4440792000000003229"}],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"jsonarray","crypt":null,"field_label":"Tag","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Tag","ui_type":209,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000125055","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":2000,"column_name":"TAGMODULEREFID","view_type":{"view":true,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Tag","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"jsonobject","crypt":null,"field_label":"Created By","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Created By","ui_type":20,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002623","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"SMCREATORID","view_type":{"view":true,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Created_By","unique":{},"data_type":"ownerlookup","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"boolean","crypt":null,"field_label":"Email Opt Out","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Email Opt Out","ui_type":301,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000014177","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":5,"column_name":"EMAILOPTOUT","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Email_Opt_Out","unique":{},"data_type":"boolean","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Skype ID","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Skype ID","ui_type":37,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000014173","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":50,"column_name":"SKYPEIDENTITY","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Skype_ID","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"jsonobject","crypt":null,"field_label":"Modified By","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Modified By","ui_type":20,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002625","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"MODIFIEDBY","view_type":{"view":true,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Modified_By","unique":{},"data_type":"ownerlookup","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Created Time","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Created Time","ui_type":200,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002627","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"CREATEDTIME","view_type":{"view":true,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Created_Time","unique":{},"data_type":"datetime","formula":{},"category":2,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Modified Time","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Modified Time","ui_type":200,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002629","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"MODIFIEDTIME","view_type":{"view":true,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Modified_Time","unique":{},"data_type":"datetime","formula":{},"category":2,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Salutation","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Salutation","ui_type":2,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000022011","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"SALUTATION","view_type":{"view":false,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":8,"api_name":"Salutation","unique":{},"history_tracking":false,"data_type":"picklist","formula":{},"category":2,"decimal_place":null,"mass_update":true,"blueprint_supported":false,"pick_list_values":[{"display_value":"-None-","actual_value":"-None-","id":"4440792000000022051"},{"display_value":"Mr.","actual_value":"Mr.","id":"4440792000000022072"},{"display_value":"Mrs.","actual_value":"Mrs.","id":"4440792000000022088"},{"display_value":"Ms.","actual_value":"Ms.","id":"4440792000000022041"},{"display_value":"Dr.","actual_value":"Dr.","id":"4440792000000022063"},{"display_value":"Prof.","actual_value":"Prof.","id":"4440792000000022038"}],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Secondary Email","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Secondary Email","ui_type":25,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000044003","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":100,"column_name":"ADDN_EMAIL","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Secondary_Email","unique":{},"data_type":"email","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Twitter","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Twitter","ui_type":22,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000053001","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":50,"column_name":"TWITTER","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Twitter","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Last Activity Time","tooltip":null,"created_source":"default","field_read_only":true,"display_label":"Last Activity Time","ui_type":786,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000052001","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"LASTACTIVITYTIME","view_type":{"view":true,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":8,"api_name":"Last_Activity_Time","unique":{},"data_type":"datetime","formula":{},"category":2,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":false,"json_type":"string","crypt":null,"field_label":"Converted Date Time","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Converted Date Time","ui_type":333,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000225696","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":120,"column_name":"CONVERTEDDATE","view_type":{"view":false,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":0,"api_name":"Converted_Date_Time","unique":{},"data_type":"datetime","formula":{},"category":2,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":false,"json_type":"boolean","crypt":null,"field_label":"Is Record Duplicate","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Is Record Duplicate","ui_type":301,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000225700","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":5,"column_name":"ISDUPLICATE","view_type":{"view":false,"edit":false,"quick_create":false,"create":false},"polymorphic_lookup":{},"subform":null,"show_type":0,"api_name":"Is_Record_Duplicate","unique":{},"data_type":"boolean","formula":{},"category":2,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Street","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Street","ui_type":1,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002635","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":250,"column_name":"LANE","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Street","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"City","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"City","ui_type":1,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002637","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":100,"column_name":"CITY","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"City","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"State","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"State","ui_type":1,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002639","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":100,"column_name":"STATE","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"State","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Zip Code","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Zip Code","ui_type":1,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002641","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":30,"column_name":"CODE","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Zip_Code","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Country","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Country","ui_type":1,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002643","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":100,"column_name":"COUNTRY","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Country","unique":{},"data_type":"text","formula":{},"category":0,"decimal_place":null,"mass_update":true,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":true,"json_type":"string","crypt":null,"field_label":"Description","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Description","ui_type":3,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000002645","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":32000,"column_name":"DESCRIPTION","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Description","unique":{},"data_type":"textarea","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}},{"system_mandatory":false,"webhook":false,"crypt":null,"field_label":"Lead Image","tooltip":null,"created_source":"default","field_read_only":false,"display_label":"Record Image","ui_type":66,"read_only":false,"association_details":null,"businesscard_supported":false,"currency":{},"id":"4440792000000152001","custom_field":false,"lookup":{},"visible":true,"refer_from_field":null,"length":255,"column_name":"PHOTO_FILEID","view_type":{"view":true,"edit":true,"quick_create":false,"create":true},"polymorphic_lookup":{},"subform":null,"show_type":7,"api_name":"Record_Image","unique":{},"data_type":"profileimage","formula":{},"category":0,"decimal_place":null,"mass_update":false,"pick_list_values":[],"multiselectlookup":{},"auto_number":{}}],"$responseHeaders":{"x-ratelimit-remaining":"99","x-ratelimit-limit":"100","x-ratelimit-reset":"1586633156357"}};
		processCompleted(currentLoadingId8);
		fields = data.fields;
		var html = "";
		for(var index in fields)
		{
			var field = fields[index];
			if(field["json_type"] == "jsonobject")
			{
				if(field.api_name && Object.prototype.toString.call(currentRecord[field.api_name]).includes("Object"))
				{
					var jsonObjKeys = Object.keys(currentRecord[field.api_name]); 
					for(var index in jsonObjKeys)
					{
						html = html + "<li id=" + field.id + "_" + index + " value=" + JSON.stringify(safetext(currentRecord[field.api_name][jsonObjKeys[index]])) + " onclick='insertFieldInName(this)'>" + field.field_label + " " + jsonObjKeys[index] + "</li>";
					}
				}
			}
			else if(field["json_type"] == "jsonarray")
			{
				if(field.api_name && Object.prototype.toString.call(currentRecord[field.api_name]).includes("Array"))
				{
					if(currentRecord[field.api_name].length != 0)
					{
						var jsonArrayKeys = Object.keys(currentRecord[field.api_name][0]); 
						for(var index in jsonObjKeys)
						{
							html = html + "<li id=" + field.id + "_" + index + " value=" + JSON.stringify(safetext(currentRecord[field.api_name].map(a => a[jsonArrayKeys[index]]))) + " onclick='insertFieldInName(this)'>" + field.field_label + " " + jsonObjKeys[index] + "</li>";
						}
					}
				}
			}
			else if(field["json_type"])
			{
				html = html + "<li id=" + field.id + " value=" + JSON.stringify(safetext(currentRecord[field.api_name])) + " onclick='insertFieldInName(this)'>" + safetext(field.field_label) + "</li>";
			}
		}
		$("#fields ul").append(html);
		html = html.replace(/insertFieldInName/g,'insertFieldInNotes');
		$("#notes-fields ul").append(html);
	});
}

function insertFieldInName(element)
{
	var $txt = jQuery("#task-name");
    var caretPos = $txt[0].selectionStart;
    var nextCursorPos = caretPos + element.getAttribute('value').length;
	$("#task-name").val(function(){
        var returnText = [this.value.slice(0, caretPos), element.getAttribute('value'), this.value.slice(caretPos)].join('');
		constructParams('name',returnText);
		return returnText;
	});
	$txt[0].selectionStart = nextCursorPos;
}

function insertFieldInNotes(element)
{
	var $txt = jQuery("#task-notes");
    var caretPos = $txt[0].selectionStart;
    var nextCursorPos = caretPos + element.getAttribute('value').length;
	$("#task-notes").val(function(){
		var returnText = [this.value.slice(0, caretPos), element.getAttribute('value'), this.value.slice(caretPos)].join('');
		constructParams('notes',returnText);
		return returnText;
	});
	$txt[0].selectionStart = nextCursorPos;
}

function save()
{
	var func_name = "asanaforzohocrm__ct";
	var req_data ={
	  "arguments": JSON.stringify(params)
	};
	var currentLoadingId8 = loadingId++;
	showProcess("Creating task",currentLoadingId8)
	ZOHO.CRM.FUNCTIONS.execute(func_name, req_data).then(function(response){
		processCompleted(currentLoadingId8);
		if(response.code == "success")
		{
			var returnMap = JSON.parse(response.details.output)
			if(returnMap.status_code == 200 || returnMap.status_code == 201 || returnMap.status_code == 204)
			{
				var currentLoadingId10 = loadingId++;
				showProcess("Task Created Successfully",currentLoadingId10)
				setTimeout(function(){processCompleted(currentLoadingId10);ZOHO.CRM.UI.Popup.close();}, 3000);
			}
			else
			{
				var currentLoadingId11 = loadingId++;
				showProcess("Failure, Please try again",currentLoadingId11)
				setTimeout(function(){processCompleted(currentLoadingId11);}, 3000);
			}
		}
		else
		{
			var currentLoadingId12 = loadingId++;
			showProcess("Failure, Please try again",currentLoadingId12)
			setTimeout(function(){processCompleted(currentLoadingId12);}, 3000);
		}
	})
}

function showMultiSelect(title, values, callback){
    $("#multiselect-window").show();
	
	if(title === 'Choose projects') {
		$('.choose-projects-filter').show();
	} else {
		$('.choose-projects-filter').hide();
	}

    $("#multiselect-window .multiselect-title").text(title);
    $('#multiselect-optholder').html("");
    optCallback = callback;
    optionsSelected = [];
    for(var i in values){
        let selected = values[i]['selected'] === true;
        if(selected){
            optionsSelected.push(values[i]['value']);
        }
        $('#multiselect-optholder').append(`<div class="multiselect-option-item ${selected ? 'selected': ''}" onclick="addSelectedOpt(event, '${safetext(values[i]['value'])}')">
                        <div class="optbox"></div> ${safetext(values[i]['label'])}
                    </div>`);
    }
}

function addSelectedOpt(e, val){
    if(optionsSelected.indexOf(val) === -1){
        $(e.target).addClass('selected');
        optionsSelected.push(val);
    }else{
        $(e.target).removeClass('selected');
        optionsSelected.splice(optionsSelected.indexOf(val),1)
    }
}

function optionSelectDone(){
    optCallback(optionsSelected);
    if($("#multiselect-window .multiselect-title").text() == "Choose followers")
    {
    	constructParams("followers",optionsSelected);
    }
    else if($("#multiselect-window .multiselect-title").text() == "Choose projects")
    {
    	constructParams("projects",optionsSelected);
    }
    else if($("#multiselect-window .multiselect-title").text() == "Choose tags")
    {
    	constructParams("tags",optionsSelected);
    }
    $("#multiselect-window").hide();
}

function safetext(text){
    var table = {
        '<': 'lt',
        '>': 'gt',
        '"': 'quot',
        '\'': 'apos',
        '&': 'amp',
        '\r': '#10',
        '\n': '#13'
    };
    if(text)
    {
    	return text.toString().replace(/[<>"'\r\n&]/g, function(chr){
	        return '&' + table[chr] + ';';
	    });
    }
    else
    {
    	return "";
    }
}

function showDatePicker()
{
	$("#due-on").hide();
	$("#dateTime").show();
	constructParams("due_on",$("#datepicker").val());
}

function hideDatePicker()
{
	$("#dateTime").hide();
	$("#due-on").show();
}