
	var dropDown = (function(config,e){
            let appDropDown = {
                config : config,
                init : function(config = null){
                    if($('.dropDownContainer.' + appDropDown.config.name).length) return;
                              this.config = config ? config : this.config;
                              config = this.config;

                  let len = this.config.values.length;
                              let parent = document.createElement('div');

                              for(let index in config.values){
                                      let div = document.createElement('div');
                                      let cont_elem = document.createElement('span');
                                      let num_val =  config.values[index].snippetId;;
                                      cont_elem.innerText = config.values[index].snippetWord;

                                      cont_elem.setAttribute('class',this.config.name + "_item");
                                      cont_elem.setAttribute('data-' + this.config.name,num_val);
                                      div.appendChild(cont_elem);
                                      parent.appendChild(div);
                  }

                  this.config.input.parentNode.appendChild(parent);
                  $(this.config.input).off('click');
                  parent.classList.add('dropDownContainer');
                  parent.classList.add(this.config.name);

                  this.config.input.addEventListener('click',appDropDown.ShowOrHide);

                              $('.dropDownContainer .' + appDropDown.config.name + "_item").parent().click(function(e){
                                      $('.snippet_added_text').hide();
                                      let snippetId = $(this).find('span').attr('data-' + appDropDown.config.name);
                                      let snippetName = snippetId.length ? appDropDown.config.values.filter(o=>o.snippetId == snippetId) : [];
                                              snippetName = snippetName.length ? snippetName[0].snippetWord : "";
                                      if(snippetId){

                                              app.replaceSnippetContent(snippetId,appDropDown.config.target);
                                              $('#dropdown_text').text(snippetName);

                                      }

                                      appDropDown.ShowOrHide(e);

                  });

                  $('.dropDownContainer.' + appDropDown.config.name).hide();

                  this.set_blur_hide();
                },
                ShowOrHide : function(e){

                  $('.dropDownContainer').not($('.dropDownContainer.' + appDropDown.config.name)).hide();
                              $('.dropDownContainer.' + appDropDown.config.name).toggle();
                              appDropDown.config.input.classList.toggle('open');

                  e.stopPropagation();
                },
                set_blur_hide : function(){
                  let close = 1;
                  $('body').on('click',function(){
                    if(close){
                                        $('.dropDownContainer.' + appDropDown.config.name).hide();
                                        appDropDown.config.input.classList.remove('open');
                                }
                    close = 1;
                  });

                  $('.dropDownContainer.' + appDropDown.config.name).on('mousedown',function(){
                    close = 0;
                  });

                  $(appDropDown.config.input).on('blur',function(){
                          if(close){
                             $('.dropDownContainer.' + appDropDown.config.name).hide();
                             appDropDown.config.input.classList.remove('open');
                          }
                   close = 1;
                  });
                },

            }
            appDropDown.init();
            if(e)
                appDropDown.ShowOrHide(e);
    });
	
	let app = {
                stripHtml : function(html)
                {
                   var tmp = document.createElement("DIV");
                   tmp.innerHTML = html;
                   return tmp.textContent || tmp.innerText || "";
                },
		setTicketComment : function(snippet_content){
			console.log('set');
                        let msg_content = this.stripHtml(snippet_content);
			$('#input-sms-content').val(msg_content);
		},
		replaceSnippetContent : function(snippetId,target_elem){
			ZOHODESK.get("ticket").then(function(response){
				let ticket_id = response.ticket.id;
				var reqObj = {
				url : `https://desk.zoho.com/api/v1/mysnippets/${snippetId}/apply?caseId=${ticket_id}`,
                                connectionLinkName : "readreceiptconnection",
				type : "POST", 
				headers:{
					"orgId" : appsConfig.UA_DESK_ORG_ID
				},
				postBody:{}
				};
				
				ZOHODESK.request(reqObj).then(function(response){
					var responseJSON = JSON.parse(response);
                                            var data = {};
                                        if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                            data = JSON.parse(responseJSON["response"]).statusMessage;
                                            console.log(data);
                                        }
					app.showSuccess = 1;
					app.setTicketComment(data.replacedContent);
				})

			});
		},
		checkIfSnippetExistsAndDeleteThenCreate : function(e){
				
			var reqObj = {
			url : `https://desk.zoho.com/api/v1/mysnippets`,
			type : "GET", 
                        connectionLinkName : "readreceiptconnection",
			headers:{
				"orgId" : appsConfig.UA_DESK_ORG_ID
			},
			postBody:{}
			};
                        
                        var contactProcess = curId++;
                          showProcess(`Fetching Snippets ...`, contactProcess);
			
			ZOHODESK.request(reqObj).then(function(response){

				var responseJSON = JSON.parse(response);
                                var data = {};
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                data = JSON.parse(JSON.parse(responseJSON["response"]).statusMessage);
                                console.log(data);
                            }
				
				let snippets = {
					name : 'my_snippets',
					target : document.getElementById('template_content'),
					input : document.getElementById('selSnippet'),
					values : data,
				};
				
				dropDown(snippets,e);
                               processCompleted(contactProcess);

			}).catch(function(err){
                            showErroWindow('Error to Fetch the Snippets');
                            console.log(err);
			});

		},	
		getTemplates : function(){
			console.log('ehr')
			
			let url = `https://desk.zoho.com/api/v1/ticketTemplates?departmentId=481095000000006907`;

			var reqObj = {
				url : url,
                                connectionLinkName : "readreceiptconnection",
				type : "GET", 
				headers:{
					"orgId" : appsConfig.UA_DESK_ORG_ID
				},
				postBody:{}
			};

			ZOHODESK.request(reqObj).then(function(response){
				console.log(response);
			}).catch(err=>{
				console.log(err);
			})

		},
		initialize : function(){
                    
			$('#selSnippet').click(function(e){
                          
                          app.checkIfSnippetExistsAndDeleteThenCreate(e);  
                        })
                        
                        
		}
	};
//	app.initialize();

function setTicketPhoneNumberInField(){
    ZOHODESK.get("ticket").then(function (response) {
        var ticketPhone = response['ticket']['phone'];
        if(valueExists(ticketPhone)){
            ticketPhone = ticketPhone.replace(/\D/g,'');
            $("#input-to-phone").val(ticketPhone);
        }
    });
}

window.onload = function () {
    ZOHODESK.extension.onload().then(function (App) {
        setTicketPhoneNumberInField();
        console.log(App);
        app.initialize();
        appsConfig.EXTENSION_ID = App.extensionID;
        initializeFromConfigParams();
        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });
        ZOHODESK.get("portal.name").then(function (response) {
            appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
        }).catch(function (err) {
            console.log(err);
        });
        ZOHODESK.get("portal.customDomainName").then(function(response){
            appsConfig.UA_DESK_CUSTOMDOMAIN_NAME = response['portal.customDomainName'];
        }).catch(function(err){
            console.log(err);
        });

    });
};
	
