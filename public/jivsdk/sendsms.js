var appsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var initialAppsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var safestringdiv = $('<div>');

function resolveCurrentProcessView() {
//    if(valueExists(appsConfig.PHONE_NUMBER)){
//        $('#input-from-phone').val(appsConfig.PHONE_NUMBER);
//    }
//    else{
//        var prefurl = `https://desk.zoho.com/support/${appsConfig.UA_DESK_PORTAL_NAME}/ShowHomePage.do#setup/marketplace/installed-extensions/${appsConfig.EXTENSION_ID}/preference`;
//        showErroWindow("App is not configured yet", `Configure <b>SMS From phone number</b> in extension preference page to start sending SMS. <br><br><a href="${prefurl}" nofollow noopener target="_blank"><div class="help-step-btn bg-green">
//                              <i class="material-icons">settings</i>  Go to preference page
//                            </div></a>`);
//    }
}
function resetPhoneNumerVal() {
    $('#phone-select-options').html("");
    $('#phone-select-label').text('Select a phone number');
    $('#phone-select-label').attr({'data-selectedval': 'null'});
}
function selectDropDownItem(elemId, val) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(val);
    $('#' + elemId).attr({'data-selectedval': val});
}

function showDropDown(elemId) {
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchPhoneNumbersAndShow();
        }
    }
    
}
function valueExists(val) {
    return val !== null && val !== undefined && val.length > 0 && val!=="null";
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Plivo Auth ID or Authtoken is Invalid <br><br> Try again with proper Plivo credentials from <a href="https://console.plivo.com/dashboard/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo dashboard</a>.');
}
function showContactsSearchPopUp(){
    $('.item-list-popup').show();
}
function searchContact(){
    searchContactsAndRender($('#contact-seach-name').val());
}
function searchContactsAndRender(searchTerm){
    if(!valueExists(searchTerm)){
        showErroWindow("Error","Enter search term");
        return;
    }
    var contactProcess = curId++;
    showProcess(`Searching contacts having phone numbers ...`, contactProcess);
                    var reqObj = {
                        url : `https://desk.zoho.com/api/v1/contacts/search?_all=*${encodeURI(searchTerm)}*&limit=99&mobile=`+encodeURI('${notEmpty}'),
                        connectionLinkName : "readreceiptconnection",
                        type : "GET", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{}
                     };
                     ZOHODESK.request(reqObj).then(function(response){
                         $("#contact-search-items").html(" ");
                         processCompleted(contactProcess);
                         var responseJSON = JSON.parse(response);
                         if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                             var data = JSON.parse(responseJSON["response"]).statusMessage;
                             if(data === ""){
                                 $("#contact-search-items").html("No contacts found. <br><br> <span class=\"c-silver\"> This search will include <br> only contacts having mobile numbers</span>");
                                 return;
                             }
                             if(!(data instanceof Object)){
                                 data = JSON.parse(data);
                             }
                             
                             var contactObjArray = data.data;
                             for(var i in contactObjArray){
                                 var obj = contactObjArray[i];
                                 var fullName = getSafeString((valueExists(obj.firstName) ? obj.firstName : "") +" " + (valueExists(obj.lastName) ? obj.lastName : ""));
                                 var itemHTML = `<div class="item-list-popup-item">
                    <span class="c-name"> ${fullName} </span> ${valueExists(obj.mobile) ? '<span class="c-phone '+(selectedReceips[obj.id+'-'+obj.mobile] ? 'alreadyadded': '')+'" onclick="selectPhoneNumber(\''+obj.id+'\', \''+obj.mobile+'\', \''+fullName+'\')"> <i class="material-icons">phone_iphone</i> '+obj.mobile+'</span>' : ''} ${valueExists(obj.phone) ? '<span class="c-phone '+(selectedReceips[obj.id+'-'+obj.phone] ? 'alreadyadded': '')+'" onclick="selectPhoneNumber(\''+obj.id+'\', \''+obj.phone+'\', \''+fullName+'\')"> <i class="material-icons">phone_local</i> '+obj.phone+'</span>' : ''}
                </div>`;
                                 $("#contact-search-items").append(itemHTML);
                             }
                         }
                     }).catch(function(err){
                         processCompleted(contactProcess);
                         console.log(err);
                     });
                }
var selectedReceips = {};
function selectPhoneNumber(id, number, name){
    $('#input-to-phone').val(number.replace(/\D/g,''));
    var receipNumberID = id+'-'+parseInt(number);
    if(!selectedReceips[receipNumberID]){
        selectedReceips[receipNumberID] = {
            'id': id,
            'name': name,
            'number': number
        }
        $('#recip-count-disp').text(Object.keys(selectedReceips).length);
        $('.to-recip-container').append(`<div class="rcp-mul-item" id="rcpmi-${receipNumberID}"><span class="rcpmi-name">${name}</span><span class="rcpmi-num">${number}</span><span class="rcpmi-close" onclick="removeRecipNumber('${receipNumberID}')">x</div></div>`);
    }
    //hideElem('.item-list-popup');alreadyadded
}
function removeRecipNumber(receipNumberID){
    delete selectedReceips[receipNumberID];
    removeElem(`#rcpmi-${receipNumberID}`);
    $('#recip-count-disp').text(Object.keys(selectedReceips).length);
}
function sendSMS(){
    //var prevVal = $('#input-to-phone').val();
    //var phoneNumber = prevVal.replace(/[^0-9\+]/g,'');
    //$('#input-to-phone').val(phoneNumber);
    var fromPhoneNumber = $('#input-from-phone').val();
    if(!valueExists(fromPhoneNumber)){
        showErroWindow("Select from number","Select from number and then proceed");
        return;
    }
    if(totalCount < 1){
        showErroWindow("Add Recipient Numbers","Add recipient numbers and then proceed");
        return;
    }
    if(!valueExists($("#input-sms-content").val().trim())){
        showErroWindow("Enter Message Content","Enter message content and then proceed");
        return;
    }
    $('#sms-prog-window').show();
    $('#msgItemsProgHolder').html('');
    var totalCount = Object.keys(selectedReceips).length;
    $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
    var messagesSentCount = 0;
    if(isTicketPage){
        var toPhoneVal = $("#input-to-phone").val();
        selectedReceips[toPhoneVal] = {
            "number": toPhoneVal,
            "name": toPhoneVal
        }
    }
    for(var recieptObjKey in selectedReceips){
        try{
            receipObj = selectedReceips[recieptObjKey];
            var prevVal = receipObj.number;
            var phoneNumber = prevVal.replace(/[^0-9\+]/g,'');
            let receiptId = receipObj['id']+'-'+parseInt(phoneNumber);
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${receiptId}" class="msgRespItem">
                  <div class="mri-label">${receipObj.name} - ${phoneNumber}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
            var msgFetchObj = {
                "method" :"POST",
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    "url":"https://api.jive.com/messaging/v1/messages",
                    "method": "POST",
                    "orgId": appsConfig.UA_DESK_ORG_ID,
                    "secContext": appsConfig.APP_UNIQUE_ID,
                    "data":{
                        "ownerPhoneNumber": fromPhoneNumber,
                        "contactPhoneNumbers": [
                          phoneNumber
                        ],
                        "body": $("#input-sms-content").val().trim(),

                      }
                })
            };

            if($('#msg-file-input') && $("#msg-file-input").get(0).files.length > 0){
                var formData = new FormData()
                formData.append('url', "https://api.jive.com/messaging/v1/messages");
                formData.append('method', 'POST');
                formData.append('orgId', appsConfig.UA_DESK_ORG_ID);
                formData.append('secContext', appsConfig.APP_UNIQUE_ID);
                formData.append('data', JSON.stringify({
                        "ownerPhoneNumber": fromPhoneNumber,
                        "contactPhoneNumbers": [
                          phoneNumber
                        ],
                        "body": $("#input-sms-content").val().trim(),

                      }));
                formData.append('file', $("#msg-file-input").get(0).files[0]);
                msgFetchObj.body = formData;
                msgFetchObj.headers = {
                    'Content-Type': 'multipart/form-data'
                };
            }          
            console.log(msgFetchObj)
            fetch("https://us-central1-ulgebra-license.cloudfunctions.net/makeGotoConnectHTTPCall", msgFetchObj)
            .then(function (response) {
                $("#WTR-PH-NUMS").removeClass('waitToResolve');
                messagesSentCount++;
                $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`); 
                return response.json();
            })
            .then(function (myJson) {
                console.log(myJson);
                //processCompleted(smsProcess);
                if(myJson.error){
                    //showErroMessage(myJson.error.error.message);
                    $(`#msg-resp-item-${receiptId} .mri-status`).html((myJson.error.error ? myJson.error.error.message : ' Failed ')+ `. Ask help <a href="https://apps.ulgebra.com/contact" target="_blank">here</a>`).css({'color':'crimson'});
                    return true;
               }
                $(`#msg-resp-item-${receiptId} .mri-status`).text('Sent').css({'color':'green'});
                if($('#sms-to-ticket').is(":checked")){
                    createOutgoingSMSTicket(myJson.id, fromPhoneNumber, phoneNumber, $("#input-sms-content").val().trim());
                }
            })
            .catch(function (error) {
               // processCompleted(smsProcess);
                //showTryAgainError();
                console.log("Error: " + error);
                $(`#msg-resp-item-${receiptId} .mri-status`).html(' Failed '+ `. Ask help <a href="https://apps.ulgebra.com/contact" target="_blank">here</a>`).css({'color':'crimson'});
            });
        }catch(ex){
            console.log(ex);
        }
    }      
            
}
function createOutgoingSMSTicket(id, fromPhoneNumber, phoneNumber, content){
    var ticketsArray = [];
    var threadsArray = [];
            ticketsArray.push({
                
                                        "subject": content,
                                        "extId": fromPhoneNumber+"::"+phoneNumber,
                                        "actor":{
                                            "extId": phoneNumber,
                                            "name": phoneNumber,
                                            "phone": phoneNumber
                                        }
                   
            });
            threadsArray.push({
                
                                        "content": content,
                                        "direction": "out",
                                        "from": fromPhoneNumber,
                                        "to": [phoneNumber],
                                        "extId": id,
                                        "extParentId": fromPhoneNumber+"::"+phoneNumber,
                                        "actor":{
                                            "name": fromPhoneNumber,
                                            "extId": fromPhoneNumber,
                                            "name": fromPhoneNumber
                                        }
                   
            });
    var reqObj = {
                url: `https://desk.zoho.com/api/v1/channels/${appsConfig.EXTENSION_ID}/import`,
                type: "POST",
                connectionLinkName : "readreceiptconnection",
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID,
                            "Content-Type": "application/json"
                        },
                        postBody:{
                            "data":{
                                "threads": threadsArray,
                                "tickets": ticketsArray
                            }
                        }
            };
            var ticketProcess = curId++;
            showProcess(`Creating Ticket for ...`+phoneNumber, ticketProcess);
            ZOHODESK.request(reqObj).then(function (response) {

                processCompleted(ticketProcess);
                var responseJSON = JSON.parse(response);
                console.log(responseJSON);
                if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 202) {
                    var trueResponseJSON = JSON.parse(responseJSON['response']);
                    if(trueResponseJSON.statusMessage.hasOwnProperty('errorCode')){
                        showErroWindow("Error Message from Desk", "Unable to perform this action. Kindly mail to ulgebra@zoho.com to raise a complaint");
                    }
                    else{
                        showErroWindow("Tickets has been created successfully", "Tickets has been created for mobile numbers "+phoneNumber);
                    }
                }
                else if(responseJSON["statusCode"] === 401){
                     showErroWindow("Error Message from Desk", "UnAuthorized to perform this action.");
                }
                else{
                     showErroWindow("Error Message from Desk", "Unable to perform this action. Kindly mail to ulgebra@zoho.com to raise a complaint");
                }
            }).catch(function (err) {
                processCompleted(ticketProcess);
                showTryAgainError();
                console.log(err);
            });
}
function resetSearch(){
    $("#contact-seach-name").val("");
    $("#contact-search-items").html("");
}
function fetchPhoneNumbersAndShow(adviseManualConfiguration) {
//    if(!valueExists(appsConfig.AUTH_ID) || !valueExists(appsConfig.AUTH_TOKEN)){
//        showErroMessage('Provide valid Plivo AUTH ID and AUTHTOKEN');
//        return;
//    }
    phoneFetched = true;
    $("#fromPhoneSelect option:first-of-type").text("Fetching phone numbers ...");
    fetch("https://us-central1-ulgebra-license.cloudfunctions.net/makeGotoConnectHTTPCall", {
        "method" :"POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            "url":"https://api.jive.com/messaging/v1/subscriptions",
            "method": "GET",
            "orgId": appsConfig.UA_DESK_ORG_ID,
            "secContext": appsConfig.APP_UNIQUE_ID
        })
    })
    .then(function (response) {
         $("#fromPhoneSelect option:first-of-type").text("Select a phone number");
        return response.json();
    })
    .then(function (myJson) {
        console.log(myJson);
        if(myJson.error){
            showErroMessage(myJson.error.error.message);
            return true;
       }
       myJson.items.forEach((item)=>{
           console.log(item);
              $('#fromPhoneSelect').append(`
                    <option value="${item.ownerPhoneNumber}">${item.ownerPhoneNumber}</option>
                </div>`);
       });
    })
    .catch(function (error) {
      console.log("Error: " + error);
    });
}
   
function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}
function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function addRawNumberToReceip(){
    if(!valueExists($("#raw-recipt-number").val().trim())){
        showErroWindow("Input valid number","Enter valid number to add");
        return;
    }
    var rawNumber = $("#raw-recipt-number").val().trim();
    selectPhoneNumber('raw-num-'+parseInt(rawNumber), rawNumber, 'No Name');
    $("#raw-recipt-number").val('');
    $('.fnuminp-outer').hide();
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function getSafeString(str){
    return safestringdiv.text(str).html();
}
function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
        showErroWindow('Kindly authorize the app again','<div style="margin-top:-20px;">Go to <b style="color:royalblue">GENERAL SETTINGS</b> tab above. Then  click <b style="color:royalblue">Revoke</b> button and authroize again. Then visit this tab. Refer the image below. <br><br> <a target="_blank" title="Click to View Image" href="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png"><img id="inconst_error_img" src="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png" style="max-width:80%;max-height:320px"/></a>');$('.error-window-title').css({'margin-top':'-30px'});
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        ZOHODESK.get("extension.config").then(function (response) {
            processCompleted(applicationProcess);
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if(!valueExists(configValue)){
                    continue;
                }
                if (configname === 'AUTH_ID') {
                    appsConfig.AUTH_ID = configValue;
                    initialAppsConfig.AUTH_ID = configValue;
                }
                if (configname === 'AUTH_TOKEN') {
                    appsConfig.AUTH_TOKEN = configValue;
                    initialAppsConfig.AUTH_TOKEN = configValue;
                }
                if (configname === 'PHONE_NUMBER') {
                    appsConfig.PHONE_NUMBER = configValue;
                    initialAppsConfig.PHONE_NUMBER = configValue;
                    fetchPhoneNumbersAndShow();
                }
                if (configname === 'APPLICATION_ID') {
                    appsConfig.APPLICATION_ID = configValue;
                    initialAppsConfig.APPLICATION_ID = configValue;
                }
                if (configname === 'APP_UNIQUE_ID') {
                    appsConfig.APP_UNIQUE_ID = configValue;
                    initialAppsConfig.APPLICATION_ID = configValue;
                }
            }
            if(valueExists(appsConfig.APP_UNIQUE_ID)){
                processCompleted(initProcessId);
            }
            else{
                setTimeout(initializeFromConfigParams, 5000);
            }
            fetchPhoneNumbersAndShow();
            resolveCurrentProcessView();
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
window.onload = function () {
    ZOHODESK.extension.onload().then(function (App) {
        console.log(App);
        appsConfig.EXTENSION_ID = App.extensionID;
        
        initializeFromConfigParams();

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });
        ZOHODESK.get("portal.name").then(function (response) {
            appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
        }).catch(function (err) {
            console.log(err);
        });
        ZOHODESK.get("portal.customDomainName").then(function(response){
            appsConfig.UA_DESK_CUSTOMDOMAIN_NAME = response['portal.customDomainName'];
        }).catch(function(err){
            console.log(err);
        });

    });
};
