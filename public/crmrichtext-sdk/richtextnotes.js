var richTextFieldId = "";
var loadingId = 0;
var moduleName;
var recordId;
var richText = "";
var records = [];
var recordIds = [];
var selectedNotesId = 0;
var notesAction = "";
// var customId = 1;
var editor;
var html_trimmed_title = "";
var html_trimmed_description = "";
var isBulkAdd = false;

document.addEventListener("DOMContentLoaded", function(event) {

	document.getElementById("loader").style.display = "none";
	document.getElementById("google_translate_element").style.display = "block";
	// editor = $("#placeHolder").Editor();
	ZOHO.embeddedApp.on("PageLoad",function(response){
		moduleName = response.Entity;
		if(Object.prototype.toString.call(response.EntityId).includes("Array"))
		{
			recordIds = response.EntityId;
			isBulkAdd = true;
		}
		else
		{
			recordId = response.EntityId;
		}
		initialise();
	})
	ZOHO.embeddedApp.init().then(function(){
		
	})
});

function showProcess(text, id)
{
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function processCompleted(id)
{
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}

function initialise()
{
	if(isBulkAdd)
	{
		showEditor();
	}
	else
	{
		saveOrFetchRichTextNotes();
	}
}

function showEditor(element)
{
	$("#editor").show();
	$("#buttons").hide();
	$(".edit-delete-buttons").hide();
	$("#input-field").val("");
	if(!element)
	{
		selectedNotesId = records.length;
		// records.push({"title":"","description":"","customId":customId++});
		records.push({"title":"","description":""});
		notesAction = "ADD";
	}
	else
	{
		selectedNotesId = element.id;
		$("#title-input-field").val(records[selectedNotesId].title);
		CKEDITOR.instances["editor1"].setData(decodeURIComponent(records[selectedNotesId].description));
		notesAction = "UPDATE";
	}
}

function deleteNotes(element)
{
	notesAction = "DELETE";
	selectedNotesId = element.id;
	saveOrFetchRichTextNotes(true);
}

function hideEditor()
{
	if(isBulkAdd)
	{
		ZOHO.CRM.UI.Popup.close().then(function(data){
		    console.log(data)
		})
	}
	$("#editor").hide();
	$("#buttons").show();
	$(".edit-delete-buttons").show();
	$("#title-input-field").val("");
	$("#description-input-field").val("");
	CKEDITOR.instances["editor1"].setData("");
	notesAction = "";
}

function updateRichText(type,value)
{
	records[selectedNotesId][type] = value;
	html_trimmed_title = value.replace(/<(.|\n)*?>/g, '');
}

function findWithAttr(array, attr, value) 
{
    for(var i = 0; i < array.length; i += 1)
    {
        if(array[i][attr] === value) 
        {
        	return array[i];
        }
    }
 }

 function saveRichTextNotes()
 {
 	records[selectedNotesId]['description'] = encodeURIComponent(CKEDITOR.instances["editor1"].getData());
	html_trimmed_description = CKEDITOR.instances["editor1"].getData().replace(/<(.|\n)*?>/g, '').replace(/[\n\r]/g, '\\n');
	saveOrFetchRichTextNotes(true);
 }

function saveOrFetchRichTextNotes(isSave)
{
	var richTextNotes = JSON.stringify(records);
	var inputMap = {"action":(isSave)?"save":"fetch","richTextFieldId":richTextFieldId,"recordId":recordId,"module":moduleName,"richTextNotes":richTextNotes.substring(1,richTextNotes.length - 1),"notesAction":notesAction,"selectedNotesId":selectedNotesId,"html_trimmed_description":html_trimmed_description,"html_trimmed_title":html_trimmed_title};
	if(recordIds.length > 0)
	{
		inputMap.recordIds = recordIds;
	}
	var func_name = "zohocrmrichtextfield__saverichtextfield";
	var req_data ={
	  "arguments": JSON.stringify(inputMap)
	};
	var currentLoadingId3 = loadingId++;
	showProcess(isSave ? (notesAction === "DELETE" ? "Deleting Note...": "Saving Note...") : "Fetching Notes...",currentLoadingId3)
	ZOHO.CRM.FUNCTIONS.execute(func_name, req_data).then(function(response){
		// var response = {"code":"success","details":{"output":"{\"zohocrmrichtextfield__Notes1\":\"{\\\"title\\\":\\\"qwer\\\",\\\"description\\\":\\\"qwer\\\",\\\"html_trimmed_title\\\":\\\"qwer\\\",\\\"html_trimmed_description\\\":\\\"qwer\\\",\\\"customId\\\":1},{\\\"title\\\":\\\"asds\\\",\\\"description\\\":\\\"\\\",\\\"html_trimmed_title\\\":\\\"asds\\\",\\\"html_trimmed_description\\\":\\\"\\\",\\\"customId\\\":2}\",\"id\":\"4474716000000046102\"}","output_type":"string","id":"376863000000710003"},"message":"function executed successfully"}
		processCompleted(currentLoadingId3);
		var output = response.details.output;
		if(response.code == "success")
		{
			if(isSave)
			{
				var currentLoadingId4 = loadingId++;
				hideEditor();
				showProcess("Completed Successfully",currentLoadingId4)
				setTimeout(function(){
					processCompleted(currentLoadingId4);
					if(isBulkAdd)
					{
						ZOHO.CRM.UI.Popup.close().then(function(data){
						    console.log(data)
						})
					}
				}, 2000);
			}
			if(output != "")
			{
				outputMap = JSON.parse(response.details.output);
				records = [];
				if(outputMap.zohocrmrichtextfield__Notes1)
				{
					records = JSON.parse("[" + outputMap.zohocrmrichtextfield__Notes1 + "]");
				}
				// customId = records.length + 1;
				richTextFieldId = outputMap.id;
				$("#rich-text-notes").html("");
				if(outputMap.zohocrmrichtextfield__Notes1)
				{
					for(var index in records)
					{
						$("#rich-text-notes").append("<div class='r-item'>" + "<div class='r-ttl'>" + getSafeString(records[index].title) + "</div>" + "<div class='r-note'>" + decodeURIComponent(records[index].description) + "</div>" + "<div class='edit-delete-buttons'>Created By <span class='r-author'><a>" + records[index].createdBy + "</a></span><span class='r-time' title='Created On "+getTimeString(new Date(records[index].createdTime).toGMTString())+"'>"+getTimeAgoString(new Date(records[index].createdTime).getTime()-1000)+"</span><button class='r-edit-btn' onclick='showEditor(this)' id=" + index + ">Edit</button>" + "<button class='r-del-btn' onclick='deleteNotes(this)' id=" + index + ">Delete</button></div></div>");
					}
				}
			}
		}
		else
		{
			var currentLoadingId12 = loadingId++;
			showProcess("Failure, Please try again",currentLoadingId12)
			setTimeout(function(){processCompleted(currentLoadingId12);}, 2000);
		}
	});
}

function getTimeAgoString(previous) {
            previous = new Date(previous);
            var msPerMinute = 60 * 1000;
            var msPerHour = msPerMinute * 60;
            var msPerDay = msPerHour * 24;
            var msPerMonth = msPerDay * 30;
            var msPerYear = msPerDay * 365;

            var elapsed = new Date() - previous;

            if (elapsed < msPerMinute) {
                 return Math.round(elapsed/1000) + ' seconds ago';
            }

            else if (elapsed < msPerHour) {
                 return Math.round(elapsed/msPerMinute) + ' minutes ago';
            }

            else if (elapsed < msPerDay ) {
                 return Math.round(elapsed/msPerHour ) + ' hours ago';
            }

            else if (elapsed < msPerMonth) {
                return ' ' + Math.round(elapsed/msPerDay) + ' days ago';
            }

            else if (elapsed < msPerYear) {
                return ' ' + Math.round(elapsed/msPerMonth) + ' months ago';
            }

            else {
                return ' ' + Math.round(elapsed/msPerYear ) + ' years ago';
            }
        }

		function getSafeString(rawStr){
    if(!rawStr || rawStr.trim() === ""){
        return "";
    }
    return $('<textarea/>').text(rawStr).html();
}



function getTimeString(time){
      let msgTime = Date.parse(time);
      let timeSuffix = "am";
      if(isNaN(msgTime) || time.indexOf('Z')<0){
        return time;
      }
      let msgHour = new Date(msgTime).getHours();
      timeSuffix = msgHour>12 ? "pm" : "am";
      msgHour = msgHour>12 ? msgHour-12 : msgHour;
      let msgMins = new Date(msgTime).getMinutes();
      msgMins = msgMins<10 ? "0"+msgMins : msgMins;
      return msgHour+":"+msgMins+" "+timeSuffix;
    }

