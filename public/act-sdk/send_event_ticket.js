var appsConfig = {
    "ACCESS_TOKEN": undefined,
    "APP_UNIQUE_ID": undefined,
    "DEPARTMENT_ID" : undefined,
};
var initialAppsConfig = {
    "ACCESS_TOKEN": undefined,
    "APP_UNIQUE_ID": undefined,
    "DEPARTMENT_ID" : undefined,
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var serverURL = "https://sms.ulgebra.com";
var customerDetailsAppendUrl = "";
var customerPreFillFields = ["lastName","firstName","email","phone"];

function fetchEventsAndShow() {
    var reqObj = {
        url: `https://acuityscheduling.com/api/v1/appointment-types`,
        type: "GET",
        headers: {
            "Authorization": (appsConfig.ACCESS_TOKEN === null ? initialAppsConfig.ACCESS_TOKEN : appsConfig.ACCESS_TOKEN)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Fetching Acuity Events ...`, applicationProcess);
    $("#result-holder").html('<div class="event-fetch-status">Loading...</div>');
    ZOHODESK.request(reqObj).then(function (response) {
        $("#result-holder").html("");
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
            var data = JSON.parse(responseJSON["response"]);
            if(data.length === 0){
                $("#result-holder").html('<div class="event-fetch-status">No Events Available</div>');
            }
            else{
              for(var i in data){
                  var item = data[i];
                  if(!item.active){
                        continue;
                  }
                  $("#result-holder").append(`<div id="${item.id}" class="event-item" style="border-color: ${item.color}">
                <div class="eventName">
                    ${safetext(item.name)}
                </div>
                <div class="eventSubType">
                    <div class="eventDuration">
                       <i class="material-icons">access_time</i> ${getHourDisplay(item.duration)}
                    </div>
                    <div class="eventType">
                        ${item.type}
                    </div>
                </div>
                <div class="eventLink">
                    <input type="text" id="inp-eventLink-${item.id}" style="position: absolute;left: -999em;" aria-hidden="true" readonly value="${item.schedulingUrl + customerDetailsAppendUrl}"/>
                    <button class="btn-copy" onclick="copyEventLink('${item.id}','${item.schedulingUrl + customerDetailsAppendUrl}')">Copy Link</button>
                </div>
            </div>`);
              }  
            }
        }
        else if(responseJSON["status_code"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showTryAgainError();
                }
    }).catch(function (err) {
        if(!valueExists(appsConfig.ACCESS_TOKEN) && !valueExists(initialAppsConfig.ACCESS_TOKEN))
        {
            showInvalidCredsError();
        }
        else
        {
            showTryAgainError();
        }
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function safetext(text){
    var table = {
        '<': 'lt',
        '>': 'gt',
        '"': 'quot',
        '\'': 'apos',
        '&': 'amp',
        '\r': '#10',
        '\n': '#13'
    };
    
    return text.toString().replace(/[<>"'\r\n&]/g, function(chr){
        return '&' + table[chr] + ';';
    });
}

function copyEventLink(eventId, eventLink){
    var copyText = document.getElementById("inp-eventLink-"+eventId);
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    $("#"+eventId+" .btn-copy").text("Copied");
}

function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
        showErroMessage('Kindly refresh the page to start the configuration.');
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        ZOHODESK.get("extension.config").then(function (response) {
            processCompleted(applicationProcess);
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if(!valueExists(configValue)){
                    continue;
                }
                if (configname === 'ACCESS_TOKEN') {
                    appsConfig.ACCESS_TOKEN = configValue;
                    initialAppsConfig.ACCESS_TOKEN = configValue;
                }
                if (configname === 'DEPARTMENT_ID') {
                    appsConfig.DEPARTMENT_ID = configValue;
                    initialAppsConfig.DEPARTMENT_ID = configValue;
                }
                if (configname === 'APP_UNIQUE_ID') {
                    appsConfig.APP_UNIQUE_ID = configValue;
                    initialAppsConfig.APP_UNIQUE_ID = configValue;
                }
            }
//            showHelpWinow(false);
            if(valueExists(appsConfig.APP_UNIQUE_ID)){
                processCompleted(initProcessId);
            }
            else{
                setTimeout(initializeFromConfigParams, 5000);
            }
            customerDetailsAppendUrl = "";
            ZOHODESK.get("ticket.contactId").then(function(response){
                if(response.status == "success")
                {
                    var reqObj = {
                        url: `https://desk.zoho.com/api/v1/contacts/` + response["ticket.contactId"],
                        type: "GET",
                        connectionLinkName: "readreceiptconnection",
                            headers:{
                                "orgId" : appsConfig.UA_DESK_ORG_ID
                            },
                        postBody: {}
                    };
                    ZOHODESK.request(reqObj).then(function (response1) {
                        var responseJSON = JSON.parse(response1);
                        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201)
                        {
                            var contactInfo = JSON.parse(responseJSON["response"]).statusMessage;
                            for(var index in customerPreFillFields)
                            {
                                if(contactInfo[customerPreFillFields[index]] != null && contactInfo[customerPreFillFields[index]] != "")
                                {
                                    customerDetailsAppendUrl = customerDetailsAppendUrl + "&" + customerPreFillFields[index] + "=" + contactInfo[customerPreFillFields[index]];
                                }
                                else
                                {
                                    customerDetailsAppendUrl = customerDetailsAppendUrl + "&" + customerPreFillFields[index] + "=";
                                }
                            }
                        }
                        fetchEventsAndShow();
                    })
                }
            })
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
window.onload = function () {
    ZOHODESK.extension.onload().then(function (App) {

        appsConfig.EXTENSION_ID = App.extensionID;

        initializeFromConfigParams();

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });

        ZOHODESK.get("portal.name").then(function (response) {
            appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
        }).catch(function (err) {
            console.log(err);
        });

    });
};

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    var prefurl = `https://desk.zoho.com/support/${appsConfig.UA_DESK_PORTAL_NAME}/ShowHomePage.do#setup/marketplace/installed-extensions/${appsConfig.EXTENSION_ID}/preference`;
        showErroWindow("App is not configured yet", `Authourise <b>Acuity</b> in extension preference page. <br><br><a href="${prefurl}" nofollow noopener target="_blank"><div class="help-step-btn bg-green">
                              <i class="material-icons">settings</i>  Go to preference page
                            </div></a>`);

}
function valueExists(val) {
    if(val!=null){
        val+="";
    }
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function getHourDisplay(totalMinutes){
    var hours = Math.floor(totalMinutes / 60);          
    var minutes = totalMinutes % 60;
    if(hours === 0){
        return minutes+"mins";
    }else{
        return hours +":"+(minutes < 10 ? "0"+minutes: minutes);
    }
}