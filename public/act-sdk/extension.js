var appsConfig = {
    "ACCESS_TOKEN": undefined,
    "APP_UNIQUE_ID": undefined,
    "DEPARTMENT_ID" : undefined,
};
var initialAppsConfig = {
    "ACCESS_TOKEN": undefined,
    "APP_UNIQUE_ID": undefined,
    "DEPARTMENT_ID" : undefined,
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var serverURL = "https://sms.ulgebra.com";
var client_id = "a6l7cEIz40Qkm369";
var client_secret = "A2xS6bGMUP2aBNMApIf3BzXlbT9Dm45IoXdFfoyh";
var redirect_uri = serverURL + "/public/callbacks/zohodesk/acuity/oauth2";
function syncInputValues() {

}
function resolveCurrentProcessView() {
    $('#input-api-key').val(appsConfig.ACCESS_TOKEN);
    if(valueExists(appsConfig.ACCESS_TOKEN))
    {
        $('#authenticate-button').html('Authourised &#10004;').prop('disabled', true);
    }
    else
    {
        $('#authenticate-button').html('Authourise').prop('disabled', false);
    }
    if (valueExists(appsConfig.ACCESS_TOKEN) && valueExists(appsConfig.DEPARTMENT_ID)) {
        $('#input-api-key').attr({'readonly':true}).val(appsConfig.ACCESS_TOKEN.substr(0,5)+"xxxxxxxxxxxxx");
        $('#dept-chooser').hide();
        $("#dept-value").text(appsConfig.DEPARTMENT_ID.substring (appsConfig.DEPARTMENT_ID.indexOf("-")+1)).css({'margin-top':'5px', 'font-size':'18px', 'font-weight':'bold'}).show();
        $(".btn-save").hide();
        fetchWebhookInfoAndExecute((function(hookId){
            if(hookId === null){
                $("#incoming-integ-status").removeClass('c-orange').removeClass('c-green').removeClass('c-crimson').addClass('c-silver').html('<i class="material-icons">error</i> Reset configuration and authourise again ');
            }else{
                $("#incoming-integ-status").removeClass('c-crimson').removeClass('c-orange').removeClass('c-silver').addClass('c-green').html('<i class="material-icons">check_circle</i> Acuity - Integration enabled');
            }
        }));
    }
    else {
        $(".btn-save").show();
        $("#dept-value").hide();
        $('#dept-chooser').show();
        $('#dept-select-label').text('Select a department');
        $('#input-api-key').removeAttr('readonly');
        if(valueExists(appsConfig.ACCESS_TOKEN) && !valueExists(appsConfig.DEPARTMENT_ID))
        {
            $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Choose Department');
        }
        else if(valueExists(appsConfig.ACCESS_TOKEN))
        {
            $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Click save to proceed');
        }
        else
        {
            $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Authourise to enable integration');
        }
    }
}
function showHelpWinow(manual){
    if(!valueExists(appsConfig.ACCESS_TOKEN)){
        showHelpItem('step-1');
    }
    else{
        if(manual){
            showHelpItem('step-youtube');
        }
    }
}
function resetPhoneNumerVal() {
    $('#phone-select-options').html("");
    $('#dept-select-label').text('Select a department');
    $('#dept-select-label').attr({'data-selectedval': 'null'});
     appsConfig.DEPARTMENT_ID = undefined;
}
function selectDropDownItem(elemId, val, displayVal) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(displayVal);
    $('#' + elemId).attr({'data-selectedval': val});
    appsConfig.DEPARTMENT_ID = val;
    $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Click save to proceed');
}

function showDropDown(elemId) {
    elemId = "phone-select-options";
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchDepartmentsAndShow();
        }
    }
    
}
function fetchDepartmentsAndShow() {
    var reqObj = {
        url: `https://desk.zoho.com/api/v1/departments`,
        type: "GET",
        connectionLinkName: "readreceiptconnection",
            headers:{
                "orgId" : appsConfig.UA_DESK_ORG_ID
            },
        postBody: {}
    };
    $('#phone-select-options').html(`<div class="statusMsg">Loading...</div>`);
    ZOHODESK.request(reqObj).then(function (response) {
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            var data = JSON.parse(responseJSON["response"]).statusMessage;
            console.log(data);
            $('#phone-select-options').html("");
            var deptArray = data.data;
                for (var i in deptArray) {
                    var obj = deptArray[i];
                        $('#phone-select-options').append(`<div class="dropdown-select-item" onclick="selectDropDownItem('dept-select-label','${obj.id+"-"+obj.name}', '${obj.name}')">${obj.name}</div>`);
                }
        }else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showErroWindow('Unable to departments', 'Try again later.');
                }
    }).catch(function (err) {
        showErroWindow('Unable to fetch Plivo mobile numbers', 'Try again later.');
        console.log(err);
    });
}
function valueExists(val) {
    if(val!=null){
        val+="";
    }
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function saveAPIKey() {
    
    if(!valueExists(appsConfig.DEPARTMENT_ID)){
        showErroWindow('Department not selected','Kindly select deparment to create event.');
        return;
    }
    if(!valueExists(appsConfig.ACCESS_TOKEN))
    {
        showErroWindow('Please authourise your accuity account');
        return;
    }
    var phoneNumberProcess = curId++;
    createCustomFields();

    var deptProcess = curId++;
    showProcess('Setting departmentId ...', deptProcess);
    ZOHODESK.set('extension.config', {name: 'DEPARTMENT_ID', value: appsConfig.DEPARTMENT_ID}).then(function (res) {
        createApplicationInMessageBird();
        console.log(res);
        processCompleted(deptProcess);
    }).catch(function (err) {
        processCompleted(authIdProcess);
        console.log(err);
    });
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Calendly API Key is Invalid <br><br> Try again with proper Calendly API Key from <a href="https://calendly.com/integrations" title="Click to go to Calendly dashboard" target="_blank" noopener nofollow>Calendly dashboard</a>.');
}


function resetAllFields(){
    if(valueExists(appsConfig.ACCESS_TOKEN)){
        ZOHODESK.set('extension.config', {name: 'ACCESS_TOKEN', value: "null"}).then(function (res) {
            ZOHODESK.set('extension.config', {name: 'DEPARTMENT_ID', value: "null"}).then(function(res){
                if(!$("#incoming-integ-status")[0].textContent.includes("Access token has been revoked. Reset configuration and authourise again"))
                {
                    fetchWebhookInfoAndExecute(deleteApplicationInMessageBird);
                }
                else
                {
                    appsConfig.ACCESS_TOKEN = null;
                    appsConfig.DEPARTMENT_ID = null;
                }
                console.log(res);
                resolveCurrentProcessView();
            })
        }).catch(function (err) {
            showTryAgainError();
            console.log(err);
        });
    }
}

function revokeAccessToken(){
    var reqObj = {
        url: `https://acuityscheduling.com/oauth2/disconnect`,
        type: "POST",
        headers: {},
        postBody: {"access_token":appsConfig.ACCESS_TOKEN,"client_id":client_id,"client_secret":client_secret}
    };
    ZOHODESK.request(reqObj).then(function(response){
        var a = "asd";
    });
}

function fetchWebhookInfoAndExecute(callback){
    var reqObj = {
        url: `https://acuityscheduling.com/api/v1/webhooks`,
        type: "GET",
        headers: {
            "Authorization": (appsConfig.ACCESS_TOKEN === null ? initialAppsConfig.ACCESS_TOKEN : appsConfig.ACCESS_TOKEN)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Checking desk application in Acuity ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
            var data = JSON.parse(responseJSON["response"]);
            if (!(data instanceof Object)) {
                data = JSON.parse(data);
            }
            if(data.length === 0){
                return callback(null,"complete");
            }
            else{
              for(var i in data){
                  var webhookJSON = data[i];
                  if(webhookJSON.target === getWebhookURL('create') || webhookJSON.target === getWebhookURL('cancel') || webhookJSON.target === getWebhookURL('reschedule')){
                      callback(webhookJSON.id,(i === data.length)?"complete":"in_progress");
                  }
              }  
            }
        }
        else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showTryAgainError();
                }
    }).catch(function (err) {
        if(!$("#incoming-integ-status")[0].textContent.includes("Access token has been revoked. Reset configuration and authourise again"))
        {
            showTryAgainError();
        }
        if(err.includes("statusCode") && JSON.parse(err).statusCode == 500)
        {
            $("#incoming-integ-status").removeClass('c-orange').removeClass('c-green').removeClass('c-crimson').addClass('c-silver').html('<i class="material-icons">error</i> Access token has been revoked. Reset configuration and authourise again ');
        }
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function getWebhookURL(eventType){
    var webHookUrl = serverURL + "/public/a?p=" + appsConfig.UA_DESK_ORG_ID + ";" + appsConfig.APP_UNIQUE_ID + ";" + appsConfig.DEPARTMENT_ID.split('-')[0] + ";" + eventType.substring(0,2);
    return webHookUrl;
}

function deleteApplicationInMessageBird(webhookId,status){
    if(!valueExists(webhookId) && status != "complete"){
        return;
    }
    var reqObj = {
        url: `https://acuityscheduling.com/api/v1/webhooks/${webhookId}`,
        type: "DELETE",
        headers: {
            "Authorization": (appsConfig.ACCESS_TOKEN === null ? initialAppsConfig.ACCESS_TOKEN : appsConfig.ACCESS_TOKEN)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Removing existing desk application in Acuity ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
            // if(status === "complete")
            // {
                // revokeAccessToken();
                // appsConfig.ACCESS_TOKEN = null;
            // }
            resolveCurrentProcessView();
            window.location.reload();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}

function createApplicationInMessageBird(){
    var callbackURL = getWebhookURL('create');
    var reqObj = {
        url: `https://acuityscheduling.com/api/v1/webhooks`,
        type: "POST",
        headers: {
            "Authorization": (appsConfig.ACCESS_TOKEN === null ? initialAppsConfig.ACCESS_TOKEN : appsConfig.ACCESS_TOKEN)
        },
        postBody: {
            "target" : callbackURL,
            "event": "appointment.scheduled"
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring desk application in Acuity ...`, applicationProcess);
    createCancelApplicationInMessageBird();
    createResheduleApplicationInMessageBird();
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            resolveCurrentProcessView(); 
        }
        else if (responseJSON["statusCode"] === 401){
            showInvalidCredsError();
        }
        else{
            showTryAgainError();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function createCancelApplicationInMessageBird(){
    var callbackURL = getWebhookURL('cancel');
    var reqObj = {
        url: `https://acuityscheduling.com/api/v1/webhooks`,
        type: "POST",
        headers: {
            "Authorization": (appsConfig.ACCESS_TOKEN === null ? initialAppsConfig.ACCESS_TOKEN : appsConfig.ACCESS_TOKEN)
        },
        postBody: {
            "target" : callbackURL,
            "event": "appointment.canceled"
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring desk application in Acuity for Cancel Event...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || (responseJSON["statusCode"] === 400 && JSON.parse(responseJSON["response"]).error == "duplicate_webhook")) {
            resolveCurrentProcessView(); 
        }
        else if (responseJSON["statusCode"] === 401 || (responseJSON["statusCode"] === 400 && JSON.parse(responseJSON["response"]).error == "duplicate_webhook")){
            showInvalidCredsError();
        }
        else{
            showTryAgainError();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function createResheduleApplicationInMessageBird(){
    var callbackURL = getWebhookURL('reschedule');
    var reqObj = {
        url: `https://acuityscheduling.com/api/v1/webhooks`,
        type: "POST",
        headers: {
            "Authorization": (appsConfig.ACCESS_TOKEN === null ? initialAppsConfig.ACCESS_TOKEN : appsConfig.ACCESS_TOKEN)
        },
        postBody: {
            "target" : callbackURL,
            "event": "appointment.rescheduled"
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring desk application in Acuity for Reschedule Event...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || (responseJSON["statusCode"] === 400 && JSON.parse(responseJSON["response"]).error == "duplicate_webhook")) {
            resolveCurrentProcessView(); 
        }
        else if (responseJSON["statusCode"] === 401 || (responseJSON["statusCode"] === 400 && JSON.parse(responseJSON["response"]).error == "duplicate_webhook")){
            showInvalidCredsError();
        }
        else{
            showTryAgainError();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function createCustomFields(){
                    {
                        var applicationProcess = curId++;
                        //showProcess(`Creating custom field Calendly-Event-ID ...`, applicationProcess);
                         var reqObj = {
                        url : `https://desk.zoho.com/api/v1/organizationFields?module=events`,
                        connectionLinkName : "readreceiptconnection",
                        type : "POST", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{
                            "displayLabel" : "Acuity-Event-ID",
                            "type" : "Text",
                            "maxLength": "15"
                          }
                        };
                        ZOHODESK.request(reqObj).then(function(response){
                             processCompleted(applicationProcess);
                            var responseJSON = JSON.parse(response);
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                               
                            }else{
                                showTryAgainError();
                            }
                        }).catch(function(err){
                            processCompleted(applicationProcess);
                            showTryAgainError();
                            console.log(err);
                        });
                     }
                     
                     // {
                     //     var applicationProcess = curId++;
                     //    //showProcess(`Creating custom field Cancel-Reason ...`, applicationProcess);
                     //     var reqObj = {
                     //    url : `https://desk.zoho.com/api/v1/organizationFields?module=events`,
                     //    connectionLinkName : "readreceiptconnection",
                     //    type : "POST", 
                     //    headers:{
                     //        "orgId" : appsConfig.UA_DESK_ORG_ID
                     //    },
                     //    postBody:{
                     //        "displayLabel" : "Cancel-Reason",
                     //        "type" : "Textarea",
                     //        "toolTip": "Cancel Reason"
                     //      }
                     //    };
                     //    ZOHODESK.request(reqObj).then(function(response){
                     //        processCompleted(applicationProcess);
                     //        var responseJSON = JSON.parse(response);
                     //        if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                
                     //        }else{
                     //            showTryAgainError();
                     //        }
                     //    }).catch(function(err){
                     //        processCompleted(applicationProcess);
                     //        showTryAgainError();
                     //        console.log(err);
                     //    });
                     // }
                }

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
        showErroWindow('Kindly authorize the app again','<div style="margin-top:-20px;">Go to <b style="color:royalblue">GENERAL SETTINGS</b> tab above. Then  click <b style="color:royalblue">Revoke</b> button and authroize again. Then visit this tab. Refer the image below. <br><br> <a target="_blank" title="Click to View Image" href="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsitant_app_resolve_acuity.png"><img id="inconst_error_img" src="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsitant_app_resolve_acuity.png" style="max-width:80%;max-height:320px"/></a>');$('.error-window-title').css({'margin-top':'-30px'});
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        ZOHODESK.get("extension.config").then(function (response) {
            processCompleted(applicationProcess);
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if(!valueExists(configValue)){
                    continue;
                }
                if (configname === 'ACCESS_TOKEN') {
                    appsConfig.ACCESS_TOKEN = configValue;
                    initialAppsConfig.ACCESS_TOKEN = configValue;
                }
                if (configname === 'DEPARTMENT_ID') {
                    appsConfig.DEPARTMENT_ID = configValue;
                    initialAppsConfig.DEPARTMENT_ID = configValue;
                }
                if (configname === 'APP_UNIQUE_ID') {
                    appsConfig.APP_UNIQUE_ID = configValue;
                    initialAppsConfig.APP_UNIQUE_ID = configValue;
                }
            }
            showHelpWinow(false);
            if(valueExists(appsConfig.APP_UNIQUE_ID)){
                processCompleted(initProcessId);
            }
            else{
                setTimeout(initializeFromConfigParams, 5000);
            }
            resolveCurrentProcessView();
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
function authenticateAcuity()
{
    if(!valueExists(appsConfig.ACCESS_TOKEN))
    {
        this.popUp = window.open("https://acuityscheduling.com/oauth2/authorize?response_type=code&scope=api-v1&client_id=" + client_id + "&state=" + appsConfig.APP_UNIQUE_ID + ";" + appsConfig.UA_DESK_ORG_ID + "&redirect_uri=" + redirect_uri,'');
        var myVar = setInterval(function(){
            if(this.popUp.closed)
            {
                var applicationProcess = curId++;
                showProcess(`Fetching application configuration ...`, applicationProcess);
                ZOHODESK.get("extension.config").then(function (response) {
                    processCompleted(applicationProcess);
                    var data = response['extension.config'];
                    for (var item in data) {
                        var configname = data[item]['name'];
                        var configValue = data[item]['value'];
                        if(!valueExists(configValue)){
                            continue;
                        }
                        if (configname === 'ACCESS_TOKEN') {
                            appsConfig.ACCESS_TOKEN = configValue;
                            initialAppsConfig.ACCESS_TOKEN = configValue;
                            $('#authenticate-button').html('Authourised &#10004;').prop('disabled', true);
                        }
                    }
                    if(valueExists(appsConfig.DEPARTMENT_ID))
                    {
                        $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Click save to proceed');
                    }
                    else
                    {
                        $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Choose Department');
                    }
                }).catch(function (err) {
                    showTryAgainError();
                    console.log(err);
                });
                    clearInterval(myVar);
                }
        }, 1000);
    }
}
window.onload = function () {
    $("body").append(`<div class="contactme">If you face any issues, kindly mail to <a href="mailto:ulgebra@zoho.com">ulgebra@zoho.com</a> or <a target="_blank" href="https://wa.me/916381504050?text=Hello%20Ulgebra,%20I%20have%20a%20query">WhatsApp Now</a><br><div class="ua-brand" style=" color: grey; "> <a href="https://www.ulgebra.com/?source=appsettings" target="_blank"> an <span class="brand-brand"> <img src="https://ulgebra.com/images/ulgebra/favs/favicon.png"> Ulgebra</span> product</a></div></div>`);
    ZOHODESK.extension.onload().then(function (App) {
        
        initializeFromConfigParams();

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });

    });
};
