var appsConfig = {
    "UA_USER_ID": undefined,
    "UA_DESK_ORG_ID": undefined,
    "UA_CF_CREATED": false,
    "UA_SNIPPET_CREATED": false,
    "UA_AUTO_RECEIPT": false
};
app = null;
var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
var loaded = 0;

function increaseViewCount() {
    ZOHODESK.get("ticket.customFields").then(function (response) {
        var readCount = response['ticket.customFields']["Ticket-Views"];
        ZOHODESK.get("ticket.id").then(function (response) {
            var ticketId = response['ticket.id'];
            ZOHODESK.get("user").then(function (response) {
                readCount = valueExists(readCount) ? parseInt(readCount) + 1 : 1;
                var userObj = {
                    "id": response.user.id,
                    "fullName": response.user.fullName,
                    "email": response.user.email,
                    "time": new Date().getTime(),
                    "index": readCount
                };
                ZOHODESK.set('database', {'key': response.user.id + "" + new Date().getTime(), 'value': userObj, 'queriableValue': 'read' + ticketId}).then(function (response) {
                    console.log(response);
                    var reqObj = {
                        url: `https://desk.zoho.com/api/v1/tickets/${ticketId}`,
                        connectionLinkName: "readreceiptconnection",
                        type: "PATCH",
                        headers: {
                            "orgId": appsConfig.UA_DESK_ORG_ID
                        },
                        postBody: {
                            "customFields": {
                                "Ticket-Views" : readCount,
                                "Last-Viewed-On" : new Date().toISOString()
                            }
                        }
                    };
                    ZOHODESK.request(reqObj).then(function (response) {
                        var responseJSON = JSON.parse(response);
                        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
                            console.log(response);
                        }
                    }).catch(function (err) {
                        console.log(err);
                    });

                }).catch(function (err) {
                    console.log(err);
                });

            }).catch(function (err) {
                console.log(err);
            });
        }).catch(function (err) {
            console.log(err);
        });

    }).catch(function (err) {
        //Error handling         
    });
}

function loadHistory(start) {
    $('.loadmoreholder').remove();
    ZOHODESK.get("ticket.id").then(function (response) {
        var ticketId = response['ticket.id'];
        var appendClass = '.toggleHisEmail';
        if (start !== 0) {
            appendClass = '#loadhere' + start;
        }
        $('.historyItem:last-of-type').before(`<div class="loadhere${start}"></div>`);
        ZOHODESK.get('database', {'queriableValue': 'read' + ticketId, 'from': start, 'limit': 99}).then(function (response) {
            var historyArr = response['database.get']['data'];
            var prevDate = "";
            var normalized = sortByKey(historyArr, 'key');
            console.log(normalized);
            for (var item in normalized) {
                var itemVal = historyArr[item]['value'];
                var date = new Date(itemVal.time);
                var hours = date.toLocaleString('en-US', {hour: 'numeric', minute: 'numeric', hour12: true});//(date.getHours() > 12 ? date.getHours() - 12 : date.getHours());
                var am = (date.getHours() > 12 ? "PM" : "AM");
                var curdate = date.getDate() + " " + months[date.getMonth()];
                if (curdate === prevDate) {
                    curdate = `<span class="c-silver">${curdate}</span>`;
                }
                prevDate = date.getDate() + " " + months[date.getMonth()];
                var timeDisp = hours;// (hours > 9 ? hours : "0" + hours) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()) + " " + am;
                var agentUrl = `https://desk.zoho.com/support/${appsConfig.UA_DESK_PORTAL_NAME}/ShowHomePage.do#setup/users-control/agents/${itemVal.id}`;
                $(appendClass).after(`<div class="historyItem">
                                                    <div class="hisAgentPhoto"></div>
                                                    <div class="hisAgentName"><a href="${agentUrl}" target="_blank">${itemVal.fullName}</a></div>
                                                    <div class="hisAgentEmail">${itemVal.email}</div>
                                                    <div class="hisAgentTime">${curdate + " " + timeDisp}</div>
                                                    <div class="hisAgentIndex">${itemVal.index}</div>
                                            </div>`);
            }
            if (historyArr.length === 99) {
                $('.historyItem:last-of-type').after(`<br><br><br><div id="loadhere${start + 99}"></div><div class="loadmoreholder"><button onclick="loadHistory(${start + 99})">Load More</button</div>`);
            }
        }).catch(function (err) {
            console.log(err);
        });
    }).catch(function (err) {
        console.log(err);
    });
}

function insertSnippetInsideReplyEditor() {
    if(!valueExists(appsConfig.UA_USER_ID)){
        var prefurl = `https://desk.zoho.com/support/${appsConfig.UA_DESK_PORTAL_NAME}/ShowHomePage.do#setup/marketplace/installed-extensions/${appsConfig.EXTENSION_ID}/preference`;
//        document.getElementById("appProgressStatus").innerHTML = (`<div class="replyEditorStatus"><span style="color:crimson">Ulgebra API Key is not configured.</span> <br><br>  <div class="errorIcon">
//                    <svg fill="#ed143d" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z"/></svg><br><br> <a href="${prefurl}" target="_blank" nofollow noopener><div class="help-step-btn bg-green">
//                              Go to preference page
//                            </div></a> </div></div>`);
    }
    else{
        //document.getElementById("appProgressStatus").innerHTML = (`<div class="replyEditorStatus">Please wait, adding Read Receipt pixel in Reply Editor. <br><br> <div class="loadingIcon"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M15.55 5.55L11 1v3.07C7.06 4.56 4 7.92 4 12s3.05 7.44 7 7.93v-2.02c-2.84-.48-5-2.94-5-5.91s2.16-5.43 5-5.91V10l4.55-4.45zM19.93 11c-.17-1.39-.72-2.73-1.62-3.89l-1.42 1.42c.54.75.88 1.6 1.02 2.47h2.02zM13 17.9v2.02c1.39-.17 2.74-.71 3.9-1.61l-1.44-1.44c-.75.54-1.59.89-2.46 1.03zm3.89-2.42l1.42 1.41c.9-1.16 1.45-2.5 1.62-3.89h-2.02c-.14.87-.48 1.72-1.02 2.48z"/></svg></div></div>`);
        console.log("Loading reply editor");
        ZOHODESK.invoke('INSERT', 'ticket.replyEditor', {value: "<br>"});
        ZOHODESK.get("ticket").then(function (response) {
            var ticket = response.ticket;
            ZOHODESK.get("user").then(function (response) {
                var user = response.user;
                var content = `<div><br/><br/><br/><br/><img src="https://read.ulgebra.com/public/read/u/${appsConfig.UA_USER_ID}/tracker/zohodesk-tickets?code=ticket-${ticket.number}-thread-${ticket.threadCount}-from-${user.email}-to-${ticket.email}"> <br/><br/></div>`;
                //setTimeout((function () {
                    ZOHODESK.invoke('INSERT', 'ticket.replyEditor', {value: content});
//                    document.getElementById("appProgressStatus").innerHTML = (`<div class="replyEditorStatus">Read Receipt pixel is added in Reply Editor.<br><br> Continue with your reply <div class="loadedicon"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"/></svg></div></div>`);
                //}), 10);
            }).catch(function (err) {
                console.log(err);
            });

        }).catch(function (err) {
            console.log(err);
        });
    }
}

window.onload = function () {

    ZOHODESK.extension.onload().then(function (App) {
        app = App;
        
        App.instance.on('ticket_replyEditor.opened', function(data){
            if(valueExists(appsConfig.UA_AUTO_RECEIPT)){
                insertSnippetInsideReplyEditor();
            }else{
                console.log('- auto read reciept for reply disabled -');
            }
        });

        $('.toggleHisEmail').on('click', (function () {
            if ($('.toggleHisEmail input:first-of-type').is(":checked")) {
                $('.hisAgentEmail').show();
            } else {
                $('.hisAgentEmail').hide();
            }
        }));
        increaseViewCount();
        loadHistory(0);
//             ZOHODESK.invoke('INSERT','ticket.replyEditor', {value : "jsf f<img src=\"hello\"/>"});
        ZOHODESK.get("extension.config").then(function (response) {
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if (configname === 'UA_USERID') {
                    appsConfig.UA_USER_ID = configValue;
                    $('#input-api-key').val(appsConfig.UA_USER_ID);
                }
                if (configname === 'UA_CF_CREATED') {
                    appsConfig.UA_CF_CREATED = configValue;
                }
                if (configname === 'UA_SNIPPET_CREATED') {
                    appsConfig.UA_SNIPPET_CREATED = configValue;
                }
                if (configname === 'UA_AUTO_RECEIPT') {
                    appsConfig.UA_AUTO_RECEIPT = configValue;
                }
                ZOHODESK.get("portal.customDomainName").then(function (response) {
                    appsConfig.UA_DESK_CUSTOMDOMAIN_NAME = response['portal.customDomainName'];
                }).catch(function (err) {
                    console.log(err);
                });
//                    resolveCurrentProcessView();
            }
        }).catch(function (err) {
            console.log(err);
        });


        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });

        ZOHODESK.get("portal.name").then(function (response) {
            appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
        }).catch(function (err) {
            console.log(err);
        });

    });
};


function valueExists(val) {
    return val !== null && val !== undefined && val.length > 0 && val!=="null";
}

function sortByKey(array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}
