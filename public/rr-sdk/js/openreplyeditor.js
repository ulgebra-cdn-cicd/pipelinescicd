            var appsConfig = {
    "UA_USER_ID": undefined,
    "UA_DESK_ORG_ID": undefined,
    "UA_CF_CREATED": false,
    "UA_SNIPPET_CREATED": false
};
app = null;
var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
function insertManualSnippetInsideReplyEditor() {
    if(!valueExists(appsConfig.UA_USER_ID)){
        var prefurl = `https://desk.zoho.com/support/${appsConfig.UA_DESK_PORTAL_NAME}/ShowHomePage.do#setup/marketplace/installed-extensions/${appsConfig.EXTENSION_ID}/preference`;
        document.getElementById("appProgressStatus").innerHTML = (`<div class="replyEditorStatus"><span style="color:crimson">Ulgebra API Key is not configured.</span> <br><br>  <div class="errorIcon">
                    <svg fill="#ed143d" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z"/></svg><br><br> <a href="${prefurl}" target="_blank" nofollow noopener><div class="help-step-btn bg-green">
                              Go to preference page
                            </div></a> </div></div>`);
    }
    else{
        document.getElementById("appProgressStatus").innerHTML = (`<div class="replyEditorStatus">Please wait, adding Read Receipt pixel in Reply Editor. <br><br> <div class="loadingIcon"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M15.55 5.55L11 1v3.07C7.06 4.56 4 7.92 4 12s3.05 7.44 7 7.93v-2.02c-2.84-.48-5-2.94-5-5.91s2.16-5.43 5-5.91V10l4.55-4.45zM19.93 11c-.17-1.39-.72-2.73-1.62-3.89l-1.42 1.42c.54.75.88 1.6 1.02 2.47h2.02zM13 17.9v2.02c1.39-.17 2.74-.71 3.9-1.61l-1.44-1.44c-.75.54-1.59.89-2.46 1.03zm3.89-2.42l1.42 1.41c.9-1.16 1.45-2.5 1.62-3.89h-2.02c-.14.87-.48 1.72-1.02 2.48z"/></svg></div></div>`);
        console.log("Loading reply editor");
        ZOHODESK.invoke('INSERT', 'ticket.replyEditor', {value: "<br>"});
        ZOHODESK.get("ticket").then(function (response) {
            var ticket = response.ticket;
            ZOHODESK.get("user").then(function (response) {
                var user = response.user;
                var content = `<div><br/><br/><br/><br/><img src="https://read.ulgebra.com/public/read/u/${appsConfig.UA_USER_ID}/tracker/zohodesk-tickets?code=ticket-${ticket.number}-thread-${ticket.threadCount}-from-${user.email}-to-${ticket.email}"> <br/><br/></div>`;
                setTimeout((function () {
                        if(appsConfig.UA_AUTO_RECEIPT){
                            ZOHODESK.invoke('INSERT', 'ticket.replyEditor', {value: ""});
                        }
                        else{
                            ZOHODESK.invoke('INSERT', 'ticket.replyEditor', {value: content});
                        }
                        
                    document.getElementById("appProgressStatus").innerHTML = (`<div class="replyEditorStatus">Read Receipt pixel is added in Reply Editor.<br><br> Continue with your reply <div class="loadedicon"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"/></svg></div></div>`);
                }), 1500);
            }).catch(function (err) {
                console.log(err);
            });

        }).catch(function (err) {
            console.log(err);
        });
    }
}

function valueExists(val) {
    return val !== null && val !== undefined && val.length > 0 && val!=="null";
}

window.onload = function () {
    document.getElementById("appProgressStatus").innerHTML = (`<div class="replyEditorStatus">Please wait, adding Read Receipt pixel in Reply Editor. <br><br> <div class="loadingIcon"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M15.55 5.55L11 1v3.07C7.06 4.56 4 7.92 4 12s3.05 7.44 7 7.93v-2.02c-2.84-.48-5-2.94-5-5.91s2.16-5.43 5-5.91V10l4.55-4.45zM19.93 11c-.17-1.39-.72-2.73-1.62-3.89l-1.42 1.42c.54.75.88 1.6 1.02 2.47h2.02zM13 17.9v2.02c1.39-.17 2.74-.71 3.9-1.61l-1.44-1.44c-.75.54-1.59.89-2.46 1.03zm3.89-2.42l1.42 1.41c.9-1.16 1.45-2.5 1.62-3.89h-2.02c-.14.87-.48 1.72-1.02 2.48z"/></svg></div></div>`);
    ZOHODESK.extension.onload().then(function (App) {
        appsConfig.EXTENSION_ID = App.extensionID;
        ZOHODESK.get("extension.config").then(function (response) {
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if (configname === 'UA_USERID') {
                    appsConfig.UA_USER_ID = configValue;
                } 
                if (configname === 'UA_AUTO_RECEIPT') {
                    appsConfig.UA_AUTO_RECEIPT = configValue;
                }
            }
            ZOHODESK.get("portal.name").then(function (response) {
                    appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
                    insertManualSnippetInsideReplyEditor();
                }).catch(function (err) {
                    console.log(err);
                });
            
        }).catch(function (err) {
            console.log(err);
        });
        
    });
};