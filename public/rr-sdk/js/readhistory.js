var appsConfig = {
    "UA_USER_ID": undefined,
    "UA_DESK_ORG_ID": undefined,
    "UA_CF_CREATED": false,
    "UA_SNIPPET_CREATED": false
};
app = null;
var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
var loaded = 0;

function loadHistory(start) {
    $('.loadmoreholder').remove();
    ZOHODESK.get("ticket.id").then(function (response) {
        var ticketId = response['ticket.id'];
        var appendClass = '.toggleHisEmail';
        if (start !== 0) {
            appendClass = '#loadhere' + start;
        }
        $('.historyItem:last-of-type').before(`<div class="loadhere${start}"></div>`);
        ZOHODESK.get('database', {'queriableValue': 'read' + ticketId, 'from': start, 'limit': 99}).then(function (response) {
            var historyArr = response['database.get']['data'];
            var prevDate = "";
            var items = [];
            for (var item in historyArr) {
                items.push(historyArr[item]['value']);
            }
            items.sort((a, b) => (a.time) - (b.time));//sortByKey(items, 'index');
            console.log(items);
            for (var item in items) {
                var itemVal = items[item];
                var date = new Date(itemVal.time);
                var hours = date.toLocaleString('en-US', {hour: 'numeric', minute: 'numeric', hour12: true});//(date.getHours() > 12 ? date.getHours() - 12 : date.getHours());
                var am = (date.getHours() > 12 ? "PM" : "AM");
                var curdate = date.getDate() + " " + months[date.getMonth()];
                if (curdate === prevDate) {
                    curdate = `<span class="c-silver">${curdate}</span>`;
                }
                prevDate = date.getDate() + " " + months[date.getMonth()];
                var timeDisp = hours;// (hours > 9 ? hours : "0" + hours) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()) + " " + am;
                var agentUrl = `https://desk.zoho.com/support/${appsConfig.UA_DESK_PORTAL_NAME}/ShowHomePage.do#setup/users-control/agents/${itemVal.id}`;
                $(appendClass).after(`<div class="historyItem">
                                                    <div class="hisAgentPhoto"></div>
                                                    <div class="hisAgentName"><a href="${agentUrl}" target="_blank">${itemVal.fullName}</a></div>
                                                    <div class="hisAgentEmail">${itemVal.email}</div>
                                                    <div class="hisAgentTime">${curdate + " " + timeDisp}</div>
                                                    <div class="hisAgentIndex">${itemVal.index}</div>
                                            </div>`);
            }
            if (historyArr.length === 99) {
                $('.historyItem:last-of-type').after(`<br><br><br><div id="loadhere${start + 99}"></div><div class="loadmoreholder"><button onclick="loadHistory(${start + 99})">Load More</button</div>`);
            }
        }).catch(function (err) {
            console.log(err);
        });
    }).catch(function (err) {
        console.log(err);
    });
}
window.onload = function () {

    ZOHODESK.extension.onload().then(function (App) {
        app = App;


        $('.toggleHisEmail').on('click', (function () {
            if ($('.toggleHisEmail input:first-of-type').is(":checked")) {
                $('.hisAgentEmail').show();
            } else {
                $('.hisAgentEmail').hide();
            }
        }));
        loadHistory(0);
//             ZOHODESK.invoke('INSERT','ticket.replyEditor', {value : "jsf f<img src=\"hello\"/>"});
        ZOHODESK.get("extension.config").then(function (response) {
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if (configname === 'UA_USERID') {
                    appsConfig.UA_USER_ID = configValue;
                    $('#input-api-key').val(appsConfig.UA_USER_ID);
                }
                if (configname === 'UA_CF_CREATED') {
                    appsConfig.UA_CF_CREATED = configValue;
                }
                if (configname === 'UA_SNIPPET_CREATED') {
                    appsConfig.UA_SNIPPET_CREATED = configValue;
                }
                ZOHODESK.get("portal.customDomainName").then(function (response) {
                    appsConfig.UA_DESK_CUSTOMDOMAIN_NAME = response['portal.customDomainName'];
                }).catch(function (err) {
                    console.log(err);
                });
//                    resolveCurrentProcessView();
            }
        }).catch(function (err) {
            console.log(err);
        });


        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });

        ZOHODESK.get("portal.name").then(function (response) {
            appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
        }).catch(function (err) {
            console.log(err);
        });

    });
};

function sortByKey(array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}
