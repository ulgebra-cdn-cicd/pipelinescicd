            var appsConfig = {
                "UA_USER_ID": undefined,
                "UA_DESK_ORG_ID" : undefined,
                "UA_CF_CREATED" : false,
                "UA_SNIPPET_CREATED" : false,
                "UA_AUTO_RECEIPT" : false
            };
            function resolveCurrentProcessView(){
                $('.process_panes').hide();
                if(appsConfig.UA_USER_ID === null){
                    $('#process_apikey').show();
                }
                else if(appsConfig.UA_CF_CREATED === null){
                     $('#cf_process').show();
                     createCustomField();
                }
                else if(appsConfig.UA_SNIPPET_CREATED === null){
                     $('#snippet_process').show();
                     checkIfSnippetExistsAndDeleteThenCreate();
                }
                else{
                    $('#process_help').show();
                }
                if(valueExists(appsConfig.UA_AUTO_RECEIPT)){
                    $('.btn-radio-choose').removeClass('chosen');
                    $("#UA_AUTO_RECEIPT-true").addClass('chosen');
                }
                else{
                    $('.btn-radio-choose').removeClass('chosen');
                    $("#UA_AUTO_RECEIPT-false").addClass('chosen');
                }
            }
            function setConfigSuccess(configName){
                ZOHODESK.set('extension.config', {name : configName, value : 'true'}).then(function(res){
                    console.log(res);
                    resolveCurrentProcessView();
                }).catch(function(err){
                    console.log(err);
                });
            }
            function toggleAutoReceipt(valToSet){
                $("#auto_receipt_statusMsg").show().text('Processing...');
                ZOHODESK.set('extension.config', {name : 'UA_AUTO_RECEIPT', value : valToSet}).then(function(res){
                                        $("#auto_receipt_statusMsg").hide();
                                        appsConfig.UA_AUTO_RECEIPT = valToSet;
                                        resolveCurrentProcessView();
                                    }).catch(function(err){
                                        console.log(err);
                                    });
            }
            function saveAPIKey(){
                var apikey = document.getElementById("input-api-key").value.trim();
                    if(apikey !== ""){
                        $('#apikey_statusMsg').css({'color':'grey'}).html("Processing...");
                        var reqObj = {
                        url : `https://read.ulgebra.com/public/api/v1/apikey/${apikey}/usage`,
                        type : "GET",
                        headers:{
                        },
                        postBody:{
                          }
                     };
                        ZOHODESK.request(reqObj).then(function (response) {
                            var responseJSON = JSON.parse(response);
                            if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
                                var data = JSON.parse(responseJSON["response"]);
                                if(data.data.length > 0){
                                    $('#apikey_statusMsg').css({'color':'red'}).html("Given API Key is already used, delete existing usage in <a href=\"https://read.ulgebra.com\" nofollow noopener target=\"_blank\">Ulgebra</a> and try again.");
                                }
                                else{
                                    ZOHODESK.set('extension.config', {name : 'UA_USERID', value : apikey}).then(function(res){
                                        console.log(res);
                                        $('#apikey_statusMsg').css({'color':'green'}).html("API Key Saved");
                                        appsConfig.UA_USER_ID = apikey;
                                        appsConfig.UA_SNIPPET_CREATED = null;
                                        resolveCurrentProcessView();
                                    }).catch(function(err){
                                        console.log(err);
                                    });
                                }
                            }
                            else {
                                $('#apikey_statusMsg').css({'color':'red'}).html("Given API Key is invalid, provide valid API key from <a href=\"https://read.ulgebra.com\" nofollow noopener target=\"_blank\">Ulgebra</a> and try again.");
                                console.log(responseJSON);
                            }
                        }).catch(function (err) {
                            $('#apikey_statusMsg').css({'color':'red'}).html("Unable to Save API Key, Try again.");
                            console.log(err);
                        });
                    }
                }
                function createCustomField(){
                    {
                        var reqObj = {
                        url : `https://desk.zoho.com/api/v1/organizationFields?module=tickets`,
                        connectionLinkName : "readreceiptconnection",
                        type : "POST", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{
                            "displayLabel" : "EMAIL-LAST-READ-ON",
                            "showToHelpCenter" : false,
                            "isEncryptedField" : false,
                            "toolTip" : "Email Last Seen On",
                            "type" : "DateTime"
                          }
                        };
                        ZOHODESK.request(reqObj).then(function(response){
                            var responseJSON = JSON.parse(response);
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                var data = JSON.parse(responseJSON["response"]).statusMessage;
                                console.log(data);
                                appsConfig.UA_CF_CREATED = true;
                                setConfigSuccess('UA_CF_CREATED');
                            }
                        }).catch(function(err){
                            console.log(err);
                        });
                    }
                    {
                         var reqObj = {
                        url : `https://desk.zoho.com/api/v1/organizationFields?module=tickets`,
                        connectionLinkName : "readreceiptconnection",
                        type : "POST", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{
                            "displayLabel" : "Ticket-Views",
                            "showToHelpCenter" : false,
                            "maxLength":9,
                            "isEncryptedField" : false,
                            "toolTip" : "Ticket views count",
                            "type" : "Number"
                          }
                        };
                        ZOHODESK.request(reqObj).then(function(response){
                            var responseJSON = JSON.parse(response);
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                var data = JSON.parse(responseJSON["response"]).statusMessage;
                                console.log(data);
                                appsConfig.UA_CF_CREATED = true;
                                //setConfigSuccess('UA_CF_CREATED');
                            }
                        }).catch(function(err){
                            console.log(err);
                        });
                     }
                     
                     {
                         var reqObj = {
                        url : `https://desk.zoho.com/api/v1/organizationFields?module=tickets`,
                        connectionLinkName : "readreceiptconnection",
                        type : "POST", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{
                            "displayLabel" : "Last-Viewed-On",
                            "showToHelpCenter" : false,
                            "isEncryptedField" : false,
                            "toolTip" : "Time of ticket last opened",
                            "type" : "DateTime"
                          }
                        };
                        ZOHODESK.request(reqObj).then(function(response){
                            var responseJSON = JSON.parse(response);
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                var data = JSON.parse(responseJSON["response"]).statusMessage;
                                console.log(data);
                                appsConfig.UA_CF_CREATED = true;
                                setConfigSuccess('UA_CF_CREATED');
                            }
                        }).catch(function(err){
                            console.log(err);
                        });
                     }
                }
                function createTicketSnippet(){
                    var reqObj = {
                        url : `https://desk.zoho.com/api/v1/mysnippets`,
                        connectionLinkName : "readreceiptconnection",
                        type : "POST", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{
                            "snippetWord": "read-receipt-tracker",
                            "expandedContent": "<div><span>Ticket - ${Cases.Request Id} </span> <br/><br/></div><div><img src=\"https://read.ulgebra.com/public/read/u/"+appsConfig.UA_USER_ID+"/tracker/zohodesk-tickets?code=ticket-${Cases.Request Id}-thread-${Cases.Thread Count}-from-${User.Email}-to-${Cases.Email}\"> <br></div>"
                        }
                     };
                     ZOHODESK.request(reqObj).then(function(response){
                         var responseJSON = JSON.parse(response);
                         if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                             var data = JSON.parse(responseJSON["response"]).statusMessage;
                             console.log(data);
                             appsConfig.UA_SNIPPET_CREATED = true;
                             setConfigSuccess('UA_SNIPPET_CREATED');
                         }
                     }).catch(function(err){
                         console.log(err);
                     });
                }
                
                function checkIfSnippetExistsAndDeleteThenCreate(){
                    var reqObj = {
                        url : `https://desk.zoho.com/api/v1/mysnippets`,
                        connectionLinkName : "readreceiptconnection",
                        type : "GET", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{}
                     };
                     ZOHODESK.request(reqObj).then(function(response){
                         var responseJSON = JSON.parse(response);
                         if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                             var data = JSON.parse(responseJSON["response"]).statusMessage;
                             if(!(data instanceof Object)){
                                 data = JSON.parse(data);
                             }
                             var snippetId = null;
                             for(var i in data){
                                 var obj = data[i];
                                 if(obj.snippetWord === "read-receipt-tracker"){
                                     snippetId = obj.snippetId;
                                     break;
                                 }
                             }
                             if(snippetId !== null){
                                 var reqObj = {
                                    url : `https://desk.zoho.com/api/v1/mysnippets/${snippetId}`,
                                    connectionLinkName : "readreceiptconnection",
                                    type : "DELETE", 
                                    headers:{
                                        "orgId" : appsConfig.UA_DESK_ORG_ID
                                    },
                                    postBody:{}
                                 };
                                 ZOHODESK.request(reqObj).then(function(response){
                                     var responseJSON = JSON.parse(response);
                                     if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===204){
                                         console.log(data);
                                         createTicketSnippet();
                                     }
                                 }).catch(function(err){
                                     console.log(err);
                                 });
                             }
                             else{
                                 createTicketSnippet();
                             }
                         }
                     }).catch(function(err){
                         console.log(err);
                     });
                }
                
            
	window.onload = function () {
	 ZOHODESK.extension.onload().then(function (App) {
             
            ZOHODESK.get("extension.config").then(function(response){
                var data = response['extension.config'];
                for (var item in data) {
                    var configname = data[item]['name'];
                    var configValue = data[item]['value'];
                    if(configname==='UA_USERID'){
                        appsConfig.UA_USER_ID = configValue;
                        $('#input-api-key').val(appsConfig.UA_USER_ID);
                    }
                    if(configname==='UA_CF_CREATED'){
                        appsConfig.UA_CF_CREATED = configValue;
                    }
                    if(configname==='UA_SNIPPET_CREATED'){
                        appsConfig.UA_SNIPPET_CREATED = configValue;
                    }
                    if(configname==='UA_AUTO_RECEIPT'){
                        appsConfig.UA_AUTO_RECEIPT = configValue;
                    }
                    resolveCurrentProcessView();
                }
            }).catch(function(err){
                console.log(err);
            });
            
             
             ZOHODESK.get("portal.id").then(function(response){
                    appsConfig.UA_DESK_ORG_ID = response['portal.id'];
                }).catch(function(err){
                    console.log(err);
                });
                
	});
};

function valueExists(val) {
    return val !== null && val !== undefined && val.length > 0 && val!=="null" && val !== false;
}