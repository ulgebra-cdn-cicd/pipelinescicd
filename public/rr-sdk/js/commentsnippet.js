			
	var appsConfig = {
		"UA_USER_ID": undefined,
		"UA_DESK_ORG_ID" : undefined,
		"UA_CF_CREATED" : false,
		"UA_SNIPPET_CREATED" : false
	};
        
        var snippetAction = "comment";
	
	var dropDown = (function(config){
      let appDropDown = {
          config : config,
          init : function(config = null){
			this.config = config ? config : this.config;
			config = this.config;
			
            let len = this.config.values.length;
			let parent = document.createElement('div');
			
			for(let index in config.values){
				let div = document.createElement('div');
				let cont_elem = document.createElement('span');
				let num_val =  config.values[index].snippetId;;
				cont_elem.innerText = config.values[index].snippetWord;

				cont_elem.setAttribute('class',this.config.name + "_item");
				cont_elem.setAttribute('data-' + this.config.name,num_val);
				div.appendChild(cont_elem);
				parent.appendChild(div);
            }
            
            this.config.input.parentNode.appendChild(parent);

            parent.classList.add('dropDownContainer');
            parent.classList.add(this.config.name);
            
            this.config.input.addEventListener('click',appDropDown.ShowOrHide);

			$('.dropDownContainer .' + appDropDown.config.name + "_item").parent().click(function(e){
				$('.snippet_added_text').hide();
				let snippetId = $(this).find('span').attr('data-' + appDropDown.config.name);
				let snippetName = snippetId.length ? appDropDown.config.values.filter(o=>o.snippetId == snippetId) : [];
					snippetName = snippetName.length ? snippetName[0].snippetWord : "";
				if(snippetId){

					app.replaceSnippetContent(snippetId,appDropDown.config.target);
					$('#dropdown_text').text(snippetName);

				}
				
				appDropDown.ShowOrHide(e);

            });

            $('.dropDownContainer.' + appDropDown.config.name).hide();

            this.set_blur_hide();
          },
          ShowOrHide : function(e){
		  
            $('.dropDownContainer').not($('.dropDownContainer.' + appDropDown.config.name)).hide();
			$('.dropDownContainer.' + appDropDown.config.name).toggle();
			appDropDown.config.input.classList.toggle('open');

            e.stopPropagation();
          },
          set_blur_hide : function(){
            let close = 1;
            $('body').on('click',function(){
              if(close){
				  $('.dropDownContainer.' + appDropDown.config.name).hide();
				  appDropDown.config.input.classList.remove('open');
			  }
              close = 1;
            });

            $('.dropDownContainer.' + appDropDown.config.name).on('mousedown',function(){
              close = 0;
            });

            $(appDropDown.config.input).on('blur',function(){
			  if(close){
				  $('.dropDownContainer.' + appDropDown.config.name).hide();
				  appDropDown.config.input.classList.remove('open');
			  }
             close = 1;
            });
          },
         
      }
      appDropDown.init();
    });
	
	let app = {
		setTicketComment : function(snippet_content){
			console.log('set')
                        if(snippetAction === 'comment'){
                            ZOHODESK.set("ticket.comment",{"comment":snippet_content}).then(function(response){
                                console.log(response)
                            }).catch(function(err){
                                console.log(err)
                                    console.log('error aded ')
                            });
                            if(app.showSuccess)
                                    setTimeout(function(){
                                            $('.snippet_added_text').text('Your snippet has been pasted in comment editor').show();
                                    },200);
                        }
                        else{
                            insertInReplyEditor(snippet_content);
                        }
		},
		replaceSnippetContent : function(snippetId,target_elem){
			ZOHODESK.get("ticket").then(function(response){
				let ticket_id = response.ticket.id;
				var reqObj = {
				url : `https://desk.zoho.com/api/v1/mysnippets/${snippetId}/apply?caseId=${ticket_id}`,
				type : "POST", 
                                connectionLinkName: "readreceiptconnection",
				headers:{
					"orgId" : appsConfig.UA_DESK_ORG_ID
				},
				postBody:{}
				};
				$(".snippet_added_text").text("Processing snippet...").show();
				ZOHODESK.request(reqObj).then(function(response){
					var responseJSON = JSON.parse(response);
                                var data = {};
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                data = JSON.parse(responseJSON["response"]).statusMessage;
                                console.log(data);
                            }
                            $(".snippet_added_text").text("Your snippet has been pasted in comment editor");
					app.showSuccess = 1;
					app.setTicketComment(data.replacedContent);
				})

			});
		},
		checkIfSnippetExistsAndDeleteThenCreate : function(){
				
			var reqObj = {
			url : `https://desk.zoho.com/api/v1/mysnippets`,
			type : "GET", 
                        connectionLinkName: "readreceiptconnection",
			headers:{
				"orgId" : appsConfig.UA_DESK_ORG_ID
			},
			postBody:{}
			};
			
			ZOHODESK.request(reqObj).then(function(response){

				var responseJSON = JSON.parse(response);
                                var data = {};
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                data = JSON.parse(JSON.parse(responseJSON["response"]).statusMessage);
                                console.log(data);
                            }
				
				let snippets = {
					name : 'my_snippets',
					target : document.getElementById('template_content'),
					input : document.getElementById('selSnippet'),
					values : data,
				};
				
				dropDown(snippets);

			}).catch(function(err){
				console.log(err);
			});

		},	
		getTemplates : function(){
			console.log('ehr')
			
			let url = `https://desk.zoho.com/api/v1/ticketTemplates?departmentId=481095000000006907`;

			var reqObj = {
				url : url,
				type : "GET", 
                                connectionLinkName: "readreceiptconnection",
				headers:{
					"orgId" : appsConfig.UA_DESK_ORG_ID
				},
				postBody:{}
			};

			ZOHODESK.request(reqObj).then(function(response){
				console.log(response);
			}).catch(err=>{
				console.log(err);
			})

		},
		initialize : function(){
			
			ZOHODESK.extension.onload().then(function (App) {
				ZOHODESK.get("extension.config").then(function(response){
					var data = response['extension.config'];
					for (var item in data) {

						var configname = data[item]['name'];
						var configValue = data[item]['value'];
						
						if(configname==='UA_USERID'){
							appsConfig.UA_USER_ID = configValue;
							$('#input-api-key').val(appsConfig.UA_USER_ID);
						}
						
						if(configname==='UA_CF_CREATED'){
							appsConfig.UA_CF_CREATED = configValue;
						}
						
						if(configname==='UA_SNIPPET_CREATED'){
							appsConfig.UA_SNIPPET_CREATED = configValue;
						}
						resolveCurrentProcessView();
					}

				}).catch(function(err){
					console.log(err);
				});

				ZOHODESK.get("portal.id").then(function(response){
					if(response){
						appsConfig.UA_DESK_ORG_ID = response['portal.id'];
						app.checkIfSnippetExistsAndDeleteThenCreate();
					}
				}).catch(function(err){
						console.log(err);
				});	
				
			});
		}
	};
	
	window.onload = function () {
            addNewFeatureHTML();
		app.initialize();
	};
        
        function insertInReplyEditor(content){
            ZOHODESK.invoke('INSERT', 'ticket.replyEditor', {value: "<br>"});
                    setTimeout((function () {
                        ZOHODESK.invoke('INSERT', 'ticket.replyEditor', {value: content});
                        $('.snippet_added_text').text('Your snippet has been pasted in reply editor').show();
                    }), 1500);
        }
        
        function setSnippetAction(action){
            snippetAction = action;
            $(".btn-radio-choose").removeClass('chosen');
            $("#add-snippet-"+action).addClass('chosen');
        }

        function addNewFeatureHTML(){
            var snippetActionChooseHTML = ` <div class="snippet_ttl">
                                                Paste Snippet to
                                            </div>
                                            
                                            <div class="snippet_add_opt_holder">
                                               <div class="btn-radio-choose chosen" id="add-snippet-comment" onclick="setSnippetAction('comment')">
                                                    Comment editor
                                                </div>
                                                <div class="btn-radio-choose" id="add-snippet-reply" onclick="setSnippetAction('reply')">
                                                    Reply editor
                                                </div>
                                            </div><br>`;
            $('body').prepend(snippetActionChooseHTML);
        }